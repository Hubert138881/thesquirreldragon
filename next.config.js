const compose = require('next-compose-plugins')
const css = require('@zeit/next-css')
const sass = require('@zeit/next-sass')
const images = require('next-images')

module.exports = compose([
    [css],
    [sass],
    [images],
])