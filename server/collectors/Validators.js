const inputLengthRange = {
    username: {
        min: 4,
        max: 15
    },
    email: {
        min: 5
    },
    password: {
        min: 8,
        max: 64
    },
    question: {
        min: 12,
        max: 64
    },
    answer: {
        min: 1,
        max: 40
    }
}
module.exports = class Validators {
    static username(content) {
        content = content.trim()

        let regex = /^([a-zA-Z0-9])+$/
        if (content == "") return "NOTHING"
        if (content.length < inputLengthRange.username.min) return "TOOSHORT"
        if (content.length > inputLengthRange.username.max) return "TOOLONG"
        if (!content.match(regex)) return "WRONGSYNTAX"
        return false
    }

    static email(content) {
        content = content.trim()

        let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if (content == "") return "NOTHING"
        if (content.length < inputLengthRange.email.min) return "TOOSHORT"
        if (!content.match(regex)) return "WRONGSYNTAX"
        return false
    }

    static password(content) {
        content = content.trim()

        if (content == "") return "NOTHING"
        if (content.length < inputLengthRange.password.min) return "TOOSHORT"
        if (content.length > inputLengthRange.password.max) return "TOOLONG"
        return false
    }
    static date([day, mth, yr]) {
        if(!day ||
            mth == "def" ||
            mth < 0 ||
            mth > 12 ||
            !yr ||
            isNaN(day) ||
            day <= 0 ||
            isNaN(yr)) return(true)
        else {
            try {
                let currYr = new Date().getFullYear()
                if(currYr - 130 >= yr || currYr - 5 <= yr) return(true)

                let days
                if(mth == 1 || mth == 3 || mth == 5 || mth == 7 || mth == 8 || mth == 10 || mth == 12) days = 31
                else days = 30
                if(mth == 2)
                    if(yr % 4) days = 28
                    else days = 29
                if(day > days) return(true)
                return false
            }
            catch(err){
               return true
            }
        }
    }
    static country(country) {
        if(!country || country == "def") return(true)
        let countries = require("../../components/utilities/countries.json")
        for(let ctr of countries)
            if(ctr.name == country) return(false)
        return true
    }
    static lang(lang) {
        if(!lang) return(true)
        let langs = require("../../components/utilities/languages.json")
        for(let lng of langs)
            if(lng.code == lang) return(false)
        return true
    }
    static gender(gender) {
        if(gender == "male" || gender == "female") return(false)
        return true
    }

    static question(content) {
        content = content.trim()
        if(content == '' && !content) return "NOTHING"
        if(content.length < inputLengthRange.question.min) return "TOOSHORT"
        if(content.length > inputLengthRange.question.max) return "TOOLONG"
        return false
    }
    static answer(content) {
        content = content.trim()
        if(content == '' && !content) return "NOTHING"
        if(content.length < inputLengthRange.answer.min) return "TOOSHORT"
        if(content.length > inputLengthRange.answer.max) return "TOOLONG"
        return false
    }
    static category(content, qDB) {
        return new Promise((resolve, reject) => {
            let conOb = []
            for(let one of content){
                conOb.push({[`categories.${one}`]: {$exists: true}})
            }
            qDB.findOne({id: "categories", $or: conOb}).then(
                resp => resp ? resolve() : reject()
            )
        })
    }

    static subcategory(content, qDB) {
        return new Promise((resolve, reject) => {
            let conOb = []
            for(let one of content){
                conOb.push({[`subcategories.${one}`]: {$exists: true}})
            }
            qDB.findOne({id: "subcategories", $or: conOb}).then(
                resp => resp ? resolve() : reject()
            )
        })
    }

    static questionId(content, qDB) {
        return new Promise((resolve, reject) => {
            if(!content) return reject(false)
            content = content.trim()
            if(content == "") return reject(false)
            return qDB.findOne({_id: content}).then((resp) => {
                if(!resp) return reject(false)
                else resolve()
            }, err => reject(false))
        })
    }

    static questionParams(data, options, qDB) {
        /**
         * options:
         * sub, cat, allowZero (true/false)
         */
        return new Promise((resolve, reject) => {
            let Q, A, B, C, D, type, cat, sub
            let trans = {}

            return new Promise(async (resolve2, reject2) => {
                Q = data.Q.trim(),
                type = parseInt(data.type)

                if(!options.noCat) {
                    cat = data.cat.trim()
                    if(cat == "") reject2("EMPTY_CAT")
                }
                
                if(options.both) {
                    await Validators.category([cat], qDB).then(
                        () => {},
                        () => Validators.subcategory([cat], qDB).then(
                            () => {},
                            () => reject2("NO_MATCHING_CAT_OR_SUB")
                        )
                    )
                }
                else {
                    if(options.cat)  {
                        cat = data.cat.trim()
                        if(cat == "") reject2("NO_CAT")
                        await Validators.category([cat], qDB).then(() => {
                            () => {},
                            err => reject2("NO_CAT")
                        })
                    }
                    if(options.sub)  {
                        sub = data.sub.trim()
                        if(sub == "") reject2("EMPTY_SUB")
                        await Validators.subcategory([sub], qDB).then(() => {
                            () => {},
                            err => reject2("NO_SUB")
                        })
                    }
                }
                resolve2()
            })
            .then(() => {
                if(Validators.question(Q)) throw "WRONG_QUESTION"
                
                switch(type) {
                    case 0:
                        if(options.allowZero)
                            trans.en = {Q}
                        else throw "WRONG_TYPE"
                        break
                    case 1:
                        A = data.A.trim(); B = data.B.trim()
                        C = data.C.trim(); D = data.D.trim()
                        if(Validators.answer(A) || Validators.answer(B) ||
                           Validators.answer(C) || Validators.answer(D)) throw "WRONG_ANSWER"
                        trans.en = {Q, A, B, C, D}
                        break
                    case 2:
                        A = data.A.trim(); B = data.B.trim()
                        if(Validators.answer(A) || Validators.answer(B)) throw "WRONG_ANSWER"
                        trans.en = {Q, A, B}
                        break
                    default:
                        throw "WRONG_TYPE"
                }
                return true
            })
            .then(() => resolve({
                Q, A, B, C, D, type, cat, sub, trans
            }))
            .catch((err) => {
                if(typeof err == "string") reject({s: err})
                reject({s: "UNEXPECTED_ERROR"})
            })
        })
    } 

    static same2elem(arr) {
        for(let i = 0; i<arr.length; i++)
            for(let j = 0; j<arr.length; j++)
                if(i == j) continue
                else if(arr[i] == arr[j]) return(true)
        return false
    }
}