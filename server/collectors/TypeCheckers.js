const avalRanks = require("../../components/utilities/ranks.json")

module.exports = (app, req, minType, userType) => {
        function checkType() {
            let i
            for(i = 0; i < avalRanks.length; i++) 
                if(avalRanks[i] == minType) break
            for(let j = i; j < avalRanks.length; j++)
                if(avalRanks[j] == userType) return(true)
            return(false)
        }
        
        if(!app && !req) return(checkType())
        else {
            //We need do get information about user
            return new Promise((resolve, reject) => {
                if(!req.session.userId) return(reject("WRONGTYPE"))

                return app.dbClient.db("main").collection("users").findOne({_id: new app.ObjectID(req.session.userId)}).then(
                    profile => {
                        if(!profile) return(reject("NOUSERFOUND"))
                        userType = profile.type
                        return checkType() ? resolve() : reject()
                    },
                    err => {reject({msg: "NOCONNECTION", err}); console.log("Blad", err)}
                )  
            })
        }
}