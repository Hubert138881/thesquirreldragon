const Validators = require("./Validators")
const typeCheckers = require("./TypeCheckers")

module.exports = class QuestionFunctions {
    constructor(app, req) {
        this.app = app
        this.req = req
    }

    addSubcategory(Q, sub, subF, cat, qDB) {
        return new Promise((resolve, reject) => {
            /**
             * cat - category (or subcategory) that question will belong to (parent)
             * sub - name of the new subcategory
             * subF - full name of subcategory
             * Q - question to answer (To enter subcategory)
             */
            typeCheckers(this.app, this.req, "moderator").then(async () => {
                return await Validators.questionParams({Q, type: 0, cat}, {allowZero: true, both: true}, qDB)
                .catch(e => {throw e})
            }).then(() => {
                //Check if such sub does not already exist and if cat does
                return qDB.findOne({$or: [
                    {id: "categories", [`categories.${sub}`]: {$exists: true}},
                    {id: "subcategories", [`subcategories.${sub}`]: {$exists: true}},
                    {$and: [
                        {id: "categories", [`categories.${cat}`]: {$exists: false}},
                        {id: "subcategories", [`subcategories.${cat}`]: {$exists: false}}
                    ]} 
                ]})
            }).then((res) => {
                if(res) {throw {s: "SUB_EXISTS"}}
                //put new subcategory to database
                let newID = this.app.crypto.randomBytes(11).toString("hex")
                return Promise.all([
                    qDB.insertOne({
                        _id: newID,
                        type: 0,
                        name: sub,
                        belongsTo: cat,
                        deleted: false,
                        deleteReason: "",
                        mustBeConfirmed: false,
                        lastModerationDate: new Date(0),
                        addedAt: new Date(Date.now()),
                        lastModerationId: this.req.session.userId,
                        translations: {en: {Q}},
                    }),
                    qDB.updateOne({id: "subcategories"}, {$set: {
                        [`subcategories.${sub}`]: {
                            owner: newID,
                            name: subF,
                            parent: cat
                        }
                    }}),
                    qDB.updateOne({id: "scheme"}, {
                        $set: {
                            [`scheme.${sub}`]: []
                        },
                        $addToSet: {
                            [`scheme.${cat}`]: sub
                        }
                    })
                ])
            }).then(() => resolve())
            .catch(err => {
                err.s ? reject({msg: err.s}) : reject(err)})
        })
        
    }
}