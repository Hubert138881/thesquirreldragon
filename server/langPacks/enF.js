module.exports = {
    "NOTHING": '(input) => `Your ${input} cannot be empty!`',
    "TOOSHORT": '(input, min) => `Your ${input} should contain at least ${min} characters!`',
    "TOOLONG": '(input, min, max) => `Your ${input} should contain no more than ${max} characters!`',
    "ALREADYEXISTS": '(input) => `Sorry, but this ${input} is already occupied!`',
    "WRONGSYNTAX": '(input) => `Your ${input} has a bad structure!`',


    "SAME": '(input) => `Your ${input} is the same as it was!`',
    "NOTSAME": '() => `Sorry but that\'s not your password!`',
    "WRONGPICTURE": `() => 'Sorry, but the picture you uploaded is corrupted!'`,
    "WRONGSIZE": `() => 'Your picture seems to be too big!'`,
    "WRONGTYPE": `() => 'Your picture has wrong extension!'`
}