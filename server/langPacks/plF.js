module.exports = {
    "NOTHING": '(input) => `Pole ${input} nie może być puste!`',
    "TOOSHORT": '(input, min) => `Pole ${input} powinno mieć przynajmniej ${min} znaków!`',
    "TOOLONG": '(input, min, max) => `Pole ${input} powinno zawierać nie więcej niż ${max} znaków!`',
    "WRONGSYNTAX": '(input) => `Pole ${input} ma złą składnię!`',
    "ALREADYEXISTS_1": '(input) => `Niestety, ale taki/a ${input} juz istnieje!`',
    "ALREADYEXISTS_2": '(input) => `Mam zle wieści - taki/a ${input} juz jest!`',
    "ALREADYEXISTS_3": '(input) => `Zla/y ${input}. Juz taki istnieje!`',

    "SAME": '(input) => `Pole ${input} nie może zostać zmienione na takie jakie było!`',
    "NOTSAME": '() => `To nie jest twoje hasło`',
    "WRONGPICTURE": `() => 'To nie jest prawidłowe zdjęcie, niestety. Prześlij jakieś inne!'`,
    "WRONGSIZE": `() => 'Zdjęcie które próbujesz przesłać jest za duże!'`,
    "WRONGTYPE": `() => 'Twoje zdjęcie nie jest wspierane. Ma złe rozszerzenie!'`
}