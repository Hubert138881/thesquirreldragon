module.exports = {
    "title": "TheSquirreldragon - A place where you'll find somebody like you",

    "welcome": "Welcome",
    "username": "Username",
    "email": "E-mail",
    "password": "Password",
    "confirmation": "Password confirmation",
    "LoginConfirm": "Confirm",
    "profilePicture": "Profile picture",
    "logout": "Log out",
    "welcome-you": "Welcome",
    "error": "Error",
    "client-id": "Client ID",
    "account-type": "Account type",
    "logged-out": "You were logged out",

    "january": "January",
    "february": "February",
    "march": "March",
    "april": "April",
    "may": "May",
    "june": "June",
    "july": "July",
    "august": "August",
    "september": "September",
    "october": "October",
    "november": "November",
    "december": "December",


    //error codes
    "E404": "(404) You've got to the road not taken...",
    "E1000": "Language pack Ajax error (Not shown from this file)",
    "E1000-1": "Language pack error connected with node file system (Not shown from this file)",
    "E1001": "Logs error. Something does not work properly",
    "E1002": "Server connection error. Please try once again",
    "E1003": "Ajax error",
    "E1004-1": "Sorry, but we have problems with connection right now!",
    "E1004-2": "Required operation does not exist",
    "E1004-3": "Sorry, but you do not have access to this data",
    "E1004-4": "You have to provide a 'newValues' parameter",
    "E1004-5": "You have to provide a 'query' parameter",
    "E1004-6": "Sorry, but an unexpected error happened to your data",
    "E1005": "Log-out error",

    //form errors
    "UNPASSEDCAPTCHA": 'Our detectors received a bot-like behaviour. Access denied. Try to refresh the page',
    "WRONGEMAILORPASSWORD": 'Your e-mail or password is incorrect. Please try again'
}