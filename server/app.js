const express = require("express")
const next = require('next')
const bodyParser = require("body-parser")
const cookieParser = require("cookie-parser")
const multer = require('multer')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session);
const MongoClient = require('mongodb').MongoClient
const axios = require("axios")
axios.defaults.withCredentials = true

const port = process.env.PORT || 3000,
      dev = process.env.NODE_ENV !== 'production'
const app = next({dev})
const handle = app.getRequestHandler()

//LOG System
const log4js = require('log4js');
log4js.configure({
    appenders: { 
        errors: {type: 'file', filename: 'logs/errorLog.log'},
        app: {type: 'file', filename: 'logs/appLog.log'}
    },
    categories: {
        errors: {appenders: ['errors'], level: ['error']},
        default: {appenders: ['app'], level: ['trace']}
    }
})

app.fs = require("fs")
app.crypto = require("crypto")
app.languages = require("../components/utilities/languages.json")
app.multer = multer
app.request = require("request")
app.errorLog = log4js.getLogger('errors')
app.appLog = log4js.getLogger('app')
app.upload = multer()
app.bcrypt = require("bcrypt")
app.serverData = require("./serverData.js")
app.clientData = require("./clientData.js")
app.ObjectID = require('mongodb').ObjectID


//const serverUri = `mongodb+srv://${app.serverData.mongoUser}:${app.serverData.mongoPassword}${app.serverData.connStr}`
const serverUri = 'mongodb://localhost:27017/'
const serverClient = new MongoClient(serverUri, { useUnifiedTopology: true, useNewUrlParser: true })
app 
    .prepare()
    .then(() => {
        const server = express()

        server.use("/", express.static("public"))

        server.use(bodyParser.json())
        server.use(bodyParser.urlencoded({extended: true}))
        server.use(cookieParser())

        server.use(session({
            secret: 'ÓV@O½-YoLì¼X1@│9TbÒcÚƒµ3▒▓6',
            store: new MongoStore({url: serverUri}),
            ttl: 40 * 3600,
            saveUninitialized: false,
            resave: false
        }))
 
        server.use(async (req, res, next) => {
            if(!app.dbClient) app.dbClient = await serverClient.connect()
            next()
        })
        server.use(async (req, res, next) => {
            if(req.session.userId && app.dbClient) {
                if(!req.user)
                    await new Promise((resolve, reject) => {
                        app.dbClient.db("main").collection("users").findOne({"_id": new app.ObjectID(req.session.userId)}).then(
                            (resp) => {
                                if(resp) resolve(resp)
                                else reject({msg: "NOUSERFOUND"})
                            },
                            (err) => reject({msg: "CONNECTIONERROR", err})
                        )
                    }).then(
                        (resp) => {
                            delete resp["_id"]
                            delete resp.allowed
                            delete resp.password
                            req.user = resp
                        },
                        (err) => {
                            //Errors occured need to log them
                            req.user = {type: null}
                        }
                    )
            }
            else req.user = {type: null}
            next()
        })

        var controllerList = [
            "Log", 
            "Lang", 
            "Database", 
            "Registration", 
            "ChangeUserData", 
            "Login", 
            "Session", 
            "Questions", 
            "Update", 
            "MongoTest",
            "Console"]; //Tu nalezy dopisac kolejne kontrolery
        controllerList.map(name => require(`./controllers/${name}Controller`)(server, app))

        server.get("*", (req, res) => {
            return handle(req, res)
        })

        server.listen(port, err => {
            if (err) throw err
            console.log(`Server's ready on ${port}`)
        })
    })
    .catch(ext => {
        console.log(ext)
        process.exit(1)
    })