const Validators = require("../collectors/Validators")
const typeChecker = require("../collectors/TypeCheckers")
const QuestionsFunctions = require("../collectors/questionFunctions")

var qDB, uDB
var letters = ["Q", "A", "B", "C", "D"]

function getRandRange(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function shuffleArr(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

module.exports = (server, app) => {
    server.post('/questionsProcess', app.upload.fields([]), (req, res) => {
        let questionFunctions = new QuestionsFunctions(app, req)
        qDB = app.dbClient.db("main").collection("questions")
        uDB = app.dbClient.db("main").collection("users")

        console.log(req.body)
        return new Promise(async (resolve, reject) => {
            let Q = req.body.Q, 
                A = req.body.A, B = req.body.B, C = req.body.C, D = req.body.D, 
                type = req.body.type, sub = req.body.sub, cat = req.body.cat, id = req.body.id, 
                reason = req.body.reason, lang = req.body.lang, amount = req.body.amount,
                additional = req.body.additional, answer = req.body.answer

            switch(req.body.option) {
                case "changeCategories":
                    /**
                     * Categories, line {"ex": "on"}
                     */
                    if(req.session.userId) {
                        //Get the categories from the database and check which of the fields
                        //in a req.body are categories
                        return qDB.findOne({id: "categories"}).then((resp => {
                            let newCath = []
                            Object.keys(req.body).map(field => 
                                Object.keys(resp.categories).map(cat => {
                                    if(field == cat) newCath.push(field)
                                })
                            )
                            return newCath
                        })).then((newCath) => {
                            //If tere are categories in an array acquire users with them
                            if(!newCath.length) {throw reject({msg: "WRONGDATA"})}
                            return uDB.updateOne({_id: new app.ObjectID(req.session.userId)}, {$set: {categories: newCath}})
                        }).then(() => resolve())
                        .catch(err => err.s ? reject({msg: err.s}) : reject(err))
                    }
                    break
                case "addSubcategory": 
                    questionFunctions.addSubcategory(Q, sub, subF, cat, qDB)
                    .then(
                        () => resolve(),
                        err => reject(err)
                    )
                    break
                case "proposeQuestion":
                    /**
                     * Q, A, B, C, D - question and answers
                     * type - type of the question (1, 2)
                     * cat - category
                     */                    
                    if(req.session.userId) {
                        Validators.questionParams({Q, A, B, C, D, type, cat}, {cat: true}, qDB)
                        .then(resp => {
                            return qDB.insertOne({
                                _id: app.crypto.randomBytes(11).toString("hex"),
                                type,
                                belongsTo: cat,
                                mustBeAsked: false,
                                mustBeConfirmed: true,
                                deleted: false,
                                deleteReason: "",
                                lastModerationDate: new Date(0),
                                addedAt: new Date(),
                                lastModerationId: "0",
                                statistics: {
                                    numOfVisits: 0,
                                    nomOfClicks: 0,
                                    numOfAnswers: 0
                                },
                                translations: resp.trans,
                                allowed: {
                                    find: [
                                        {"moderator": []},
                                        {[req.session.id]: ["statistics"]},
                                        ["_id", "text", "type", "mustBeAsked"],
                                        []
                                    ],
                                    delete: [
                                        ["moderator"],
                                        []
                                    ]
                                }})
                        }).then(() => resolve())
                        .catch(err => err.s ? reject({msg: err.s}) : reject(err))
                    }
                    else return reject({msg: "NOPERMS"})
                    break
                case "getNotAccepted":
                    /**
                     * amount - amount of questions to get
                     */
                    try{
                        amount = parseInt(req.body.amount)
                        if(amount > 5 || amount < 1) {throw false}
                    }
                    catch(e){return reject({msg: "WRONGDATA"})}

                    typeChecker(app, req, "moderator").then(() => {
                        return qDB.find({
                            //mustBeConfirmed: true,
                            deleted: false,
                            $nor: [{type: 0}],
                            //lastModerationDate: {$lte: new Date(Date.now() - 1000 * 60 * 7)}
                        }).limit(amount).toArray()
                    }).then((resp) => {
                        let orArr = []
                        resp.map(qst => orArr.push({_id: qst["_id"]}))
                        return Promise.all([
                            resp,
                            orArr.length ? qDB.updateMany({$or: orArr}, {$set: {
                                lastModerationDate: new Date(),
                                lastModerationId: req.session.userId
                            }}) : void(0)
                        ])
                    }).then((resp) => resolve(resp[0]))
                    .catch(err => err.s ? reject({msg: err.s}) : reject(err))
                    break

                case "extendTime": 
                    typeChecker(app, req, "moderator").then(() => {
                        if(id != "" && !id) {throw {msg: "WRONGDATA"}}
                        return qDB.updateOne({
                            _id: id
                        }, {$set: {lastModerationDate: new Date()}})
                    }).then((resp) => {
                        if(!resp) {throw {msg: "WRONGDATA"}}
                        else resolve()
                    })
                    .catch(err => err.s ? reject({msg: err.s}) : reject(err))
                    break

                case "acceptQuestion":
                    /**
                     * id - question id
                     * cat - new category
                     * sub - new subcategory
                     * type - new type of question
                     * Q, A, B, C, D - new question and answer
                     */
                    typeChecker(app, req, "moderator").then(async () => {                      
                        return Promise.all([
                            Validators.questionParams({Q, A, B, C, D, type, cat}, {both: true}, qDB),
                            Validators.questionId(id, qDB),
                            qDB.findOne({
                                _id: id,
                                //mustBeConfirmed: true,
                                deleted: false,
                                //lastModerationDate: {$lte: new Date(Date.now() - 1000 * 60 * 7)},
                                lastModerationId: req.session.userId
                            })
                        ])
                    }).then((resp) => {
                        if(!resp[2]) {throw false}
                        return Promise.all([
                            qDB.updateOne({_id: id}, {$set: {
                                belongsTo: resp[0].cat,
                                translations: resp[0].trans,
                                mustBeConfirmed: false,
                                lastModerationDate: new Date()
                            }}),
                            qDB.updateOne({id: "scheme"}, {$push: {["scheme."+cat]: id}})
                        ]) 
                    }).then(() => resolve())
                    .catch(err => err.s ? reject({msg: err.s}) : reject(err))
                    break
 
                case "declineQuestion":
                    /**
                     * id - id of the question we want to decline
                     * reason - why it gonna be declined?
                     */
                    typeChecker(app, req, "moderator").then(() => {
                        if(!id || id.trim() == "" ||
                           !reason || reason.trim() == ""){throw false}
                        return qDB.findOne({_id: id, $nor: [{type: 0}], deleted: false})
                    }).then((resp) => {
                        if(!resp){throw false}
                        return qDB.updateOne({_id: id}, {$set: {
                            mustBeConfirmed: false,
                            deleted: true, 
                            deleteReason: reason
                        }})
                    }).then(() => resolve())
                    .catch(err => err.s ? reject({msg: err.s}) : reject(err))
                    break

                case "getMostAccurate":
                    if(req.session.userId){
                        let user, asked
                        uDB.findOne({
                            _id: new app.ObjectID(req.session.userId)
                        }).then((resp) => {
                            if(!resp) {throw false}
                            user = resp

                            asked = user.answeredQst
                            if(!asked["_must"]) return false
                            //First we need to make sure if no of the must have questions left
                            return qDB.findOne({
                                _id: {$nin: asked["_must"]},
                                mustBeAsked: true
                            })
                        }).then((resp) => {
                            if(resp) {throw {success: resp}}
                            //Get the question scheme
                            return qDB.findOne({"id": "scheme"})
                        }).then(async (scheme) => {
                            scheme = scheme.scheme
                            //Join and randomize cats and subs
                            let randSubArr = shuffleArr([...user.categories, ...user.subcategories])

                            //get array of filtered quesions (from questions that were asked)
                            let randQstArr
                            for(let randSub of randSubArr) {
                                randQstArr = scheme[randSub].filter((item => {
                                    if(asked[randSub]) {
                                        let contains = false
                                        asked[randSub].map(asked => {
                                            if(asked.id == item) contains = true
                                        })
                                        return !contains
                                    }
                                }))
                                if(randQstArr[0]) break
                            }
                            let randQst = randQstArr[getRandRange(0, randQstArr.length)]
                            if(!randQst) {throw false}
                            else {throw {success: randQst}}
                        })
                        .catch(err => { 
                            if(err.success) 
                                qDB.findOne({_id: err.success}).then(
                                    resp => {
                                        qDB.updateOne({_id: err.success}, {$inc: {"statistics.numOfVisits": 1}})
                                        resolve(resp)
                                    },
                                    err => reject(err)
                                )
                            else reject(err)
                        })
                    }
                    else reject({msg: "NOPERMISSIONS"})
                    break
                case "answerQuestion": 
                    /**
                     * answer - answer letter
                     * id - id of the question to answer
                     * cat - category
                     */
                    let qstDetails, userId = app.ObjectID(req.session.userId)

                    if(req.session.userId) {
                        if(answer != 'A' && answer != 'B' && answer != 'C' && answer != 'D')
                            return reject(false)
                        qDB.findOne({_id: id}).then((resp) => {
                            if(!resp) throw false
                            qstDetails = resp
                            
                            return Promise.all([
                                uDB.updateOne({_id: userId}, 
                                    {$addToSet: {[`answeredQst.${qstDetails.belongsTo}`]: {
                                        id, answer
                                    }}},
                                    resp.type ? {$addToSet: {"subcategories": qstDetails.name}} : void(0)
                                ),
                                qDB.updateOne({_id: id}, {$inc: {"statistics.numOfAnswers": 1}})
                            ])
                        }).then(() => resolve())
                        .catch(err => err.s ? reject({msg: err.s}) : reject(err))
                    }
                    else reject(false)
                    break
                case "addQuestion":
                    /**
                     * Q, A, B, C, D - question and answers
                     * type - type of the question (1, 2)
                     * cat - category
                     */
                    typeChecker(app, req, "moderator").then(() => {
                        return Validators.questionParams({Q, A, B, C, D, type, cat}, {both: true}, qDB)
                    }).then((resp) => {
                        return qDB.insertOne({
                            _id: app.crypto.randomBytes(11).toString("hex"),
                            type,
                            belongsTo: cat,
                            mustBeAsked: false,
                            mustBeConfirmed: false,
                            deleted: false,
                            deleteReason: "",
                            lastModerationDate: new Date(),
                            addedAt: new Date(),
                            lastModerationId: req.session.userId,
                            statistics: {
                                numOfVisits: 0,
                                nomOfClicks: 0,
                                numOfAnswers: 0
                            },
                            translations: resp.trans,
                            allowed: {
                                find: [
                                    {"moderator": []},
                                    {[req.session.id]: ["statistics"]},
                                    ["_id", "text", "type", "mustBeAsked"],
                                    []
                                ],
                                delete: [
                                    ["moderator"],
                                    []
                                ]
                            }})
                    }).then(() => resolve())
                    .catch(err => err.s ? reject({msg: err.s}) : reject(err))
                    break 
                /*Standard user options*/
                case "modifyQuestion":
                    /**
                     * type - (sub/trans) - tells what to modify
                     * sub: cat - new category
                     * trans: (prefix: lang_)  Q, A, B, C, D - new question details
                     */
                    let qDetails
                    typeChecker(app, req, "moderator").then(() => {
                        return qDB.findOne({_id: id})
                    }).then(resp => {
                        if(!resp) {throw "WRONG_ID"}
                        qDetails = resp

                        switch(type) {
                            case "sub":
                                return Validators.category([cat], qDB).then(
                                    () => {},
                                    () => Validators.subcategory([cat], qDB).then(
                                        () => {},
                                        () => {throw "NO_SUBCATEGORY"}
                                    )
                                )
                            case "trans":
                                return Promise.all([
                                    Validators.lang(lang),
                                    Validators.questionParams({Q, A, B, C, D, type: resp.type}, {noCat: true})
                                ])
                            default:
                                throw "WRONG_TYPE"
                        }
                    }).then((resp) => { 
                        switch(type) {
                            case "sub":
                                return Promise.all([
                                    qDB.updateOne({id: "scheme"}, {
                                        $pull: {[`scheme.${qDetails.belongsTo}`]: id},
                                        $addToSet: {[`scheme.${cat}`]: id}
                                    }), 
                                    qDB.updateOne({_id: id}, {$set: {belongsTo: cat}}),
                                    uDB.updateMany({clientId: {$exists: true}}, {
                                        $pull: {[`answeredQst.${qDetails.belongsTo}`]: id},
                                        $addToSet: {[`answeredQst.${cat}`]: id}
                                    })
                                ])
                            case "trans":
                                if(resp[0]) throw "WRONG_LANG"
                                return qDB.updateOne({_id: id}, {$set: {[`translations.${lang}`]: {
                                    Q, A, B, C, D
                                }}})
                        }
                    }).then(() => resolve())
                    .catch(err => err.s ? reject({msg: err.s}) : reject(err))
                    break
                case "flagQuestion":
                    if(req.session.userId) {
                        if(reason.trim() == "" || !reason|| !id || 
                          Validators.lang(lang)) return reject({msg: "WRONGDATA"})

                        else qDB.findOne({_id: id}).then(resp => {
                            if(!resp) {throw {msg: "WRONGDATA"}}
                            return qDB.updateOne({id: "flagged"}, {$push: {
                                [`flagged.${id}`]: { user: req.session.userId, reason, lang }
                            }})
                        }).then(() => resolve())
                        .catch(err => reject(err))
                    }
                    break
                case "getFlagged":
                    let flagged = {}
                    let qstIdArr = req.body.qstIdArr ? req.body.qstIdArr : []

                    typeChecker(app, req, "moderator").then(() => {
                        return qDB.findOne({id: "flagged"})
                    }).then(resp => {
                        flagged = resp.flagged
                        //Get an array of sorted flagged questions' ids
                        let sorted = Object.keys(resp.flagged).sort((a, b) => {
                            if(resp.flagged[a].length < resp.flagged[b].length)
                              return 1
                            return -1  
                        }).filter(item => !qstIdArr.includes(item)).slice(0, amount)
                        //Get them from database
                        return Promise.all([
                            qDB.find({_id: {$in: sorted}}).toArray(),
                            sorted.length ? qDB.updateMany({
                                _id: {$in: sorted}, 
                                lastModerationDate: {$lte: new Date(Date.now() - 1000 * 60 * 7)}}, 
                            {$set: {
                                lastModerationDate: new Date(),
                                lastModerationId: req.session.userId
                            }}) : void(0)
                        ])
                    }).then(resp => {
                        //Add information such as reason, language and user id
                        let combined = []
                        resp[0].map(qst => combined.push({...qst, flags: flagged[qst["_id"]]}))
                        resolve(combined)
                    }) 
                    .catch(err => reject(err))
                    break
                case "flaggedModified":
                    typeChecker(app, req, "moderator").then(() => {
                        return qDB.updateOne({id: "flagged"}, {$unset: {[`flagged.${id}`]: ""}})
                    }).then(() => resolve())
                    .catch(err => reject(err))
                    break
                case "getNotTranslated":
                    let result = {}
                    typeChecker(app, req, "moderator").then(() => {                        
                        return qDB.find({
                            $or: Object.keys(additional).map(lang => {
                                return {[`translations.${lang}`]: {$exists: false}}
                            }), 
                            lastModerationDate: {$lte: new Date(Date.now() - 1000 * 60 * 7)},
                            mustBeConfirmed: false
                        }).limit(parseInt(amount)).toArray()
                    }).then(resp => {
                        result = resp
                        return qDB.updateMany({
                            _id: {$in: resp.map(qst => {return qst["_id"]})}
                        }, {$set: {
                            lastModerationDate: new Date(),
                            lastModerationId: req.session.userId
                        }})
                    }).then(() => resolve(result))
                    .catch(err => reject(err))
                    break
                case "acceptTranslation":
                    /**
                     * Q, A, B, C, D
                     * lang, id
                     */
                    typeChecker(app, req, "moderator").then(() => {
                        if (Validators.lang(lang)) {throw false}
                        console.log("b")
                        return Promise.all([
                            Validators.questionParams({Q, A, B, C, D, type}, {noCat: true, allowZero: true}, qDB),
                            qDB.findOne({_id: id, mustBeConfirmed: false})
                        ])
                    }).then((resp) => {
                        console.log("c")
                        if (!resp[1]) {throw false}
                        return qDB.updateOne({_id: id}, {$set: {
                            ...(() => {
                                let transArr = {}
                                letters.map(letter => {
                                    if(resp[0][letter]) transArr[`translations.${lang}.${letter}`] = resp[0][letter]
                                })
                                console.log(transArr)
                                return transArr
                            })()
                        }})
                    }).then(() => resolve())
                    .catch(err => reject(err))
                    break

                case "removeTranslation":
                    typeChecker(app, req, "moderator").then(
                        () => {
                            if (req.body.id != "" && req.body.id && req.body.lang != "en")
                                app.dbClient.db("main").collection("questions").findOne({
                                    _id: req.body.id,
                                    mustBeConfirmed: false 
                                }).then( 
                                    resp => {
                                        if(!resp) return reject({msg: "NOTACCEPTED"})
                                        else {
                                            app.dbClient.db("main").collection("questions").updateOne({
                                                _id: req.body.id,
                                            }, {
                                                $unset: {[`translations.${req.body.lang}`]: ""}
                                            }).then(
                                                () => resolve({msg: "CHANGESACCEPTED"}),
                                                err => reject(err)
                                            )
                                        }
                                    },
                                    err => reject({msg: "NOCONNECTION", err})
                                )
                            else return reject({msg: "WRONGDATA"})
                        },
                        err => reject(err)
                    )
                    break
            }
        }).then(
            (data) => {
                console.log("resolved")
                res.send({err: false, data})
            },
            err => {
                console.log(err)
                res.send({err: true})
            }
        )
    })
}