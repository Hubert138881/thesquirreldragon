const inputValidation = require("../collectors/inputValidation")

module.exports = (server, app) => {
    server.post("/registration", app.upload.fields([]), async (req, res) => {

        return new Promise(async (resolve, reject) => {
            inputValidation(app, req).then(
                () => {
                    app.bcrypt.hash(req.body.password, 10).then( 
                        (hash) => {
                            let objectID = new app.ObjectID(),
                                clientId = app.crypto.randomBytes(15).toString("hex"),
                                activationLink = app.crypto.randomBytes(10).toString("hex")

                            app.dbClient.db("main").collection("users").insertOne({
                                _id: objectID,
                                clientId,
                                categories: [],
                                subcategories: [],
                                email: req.body.email,
                                username: req.body.username,
                                type: "standard",
                                password: hash,
                                profilePictureExt: "",
                                answeredQst: {},
                                activationLink,
                                loginNumber: 0,
                                registrationTime: new Date(),
                                termsAccepted:  false,
                                allowed: {
                                    find: [
                                        {"moderator": []},
                                        {[objectID.toHexString()]: []},
                                        ["clientId", "email", "username", "type", "profilePictureExt", "loginNumber", "registrationTime", "questionAsked"],
                                        ["clientId", "email", "username", "type", "profilePictureExt", "registrationTime"]
                                    ],
                                    delete: [
                                        ["moderator"],
                                        [objectID.toHexString()]
                                    ]
                                }
                            }).then(
                                () => app.fs.mkdir(`public/files/users/${clientId}`, () => {
                                    resolve()
                                }),
                                () => reject({msg: "CONNECTIONERROR", err})
                            )
                        },
                        (err) => reject({msg: "HASHERROR", err})
                    )
                },
                (validations) => reject(validations)
            )
        }).then(
            () => res.send({err: false}),
            (err) => {
                //Need to log err.err
                switch(err.msg){
                    case "CONNECTIONERROR":
                        res.send({err: true})
                        break
                    case "INPUTERROR":
                        res.send({err: true, validations: err.err})
                        break
                    case "HASHERROR":
                        res.send({err: true})
                        break
                }
            }
        )
    })
}