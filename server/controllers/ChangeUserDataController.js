const inputValidation = require("../collectors/inputValidation")
const jimp = require("jimp")

function getScale(bitmap, longerSide) {
    let longer = bitmap.width
    if (bitmap.width < bitmap.height) longer = bitmap.height
    return longerSide / longer
}

module.exports = (server, app) => {
    var fileUpload = app.multer({dest: "server/uploads"})
    server.post("/changeUserPicture", fileUpload.single("profilePicture"), (req, res) => {
        if (req.file) {
            return new Promise((resolve, reject) => {
                if (req.user.type) {
                    app.dbClient.db("main").collection("users").findOne({_id: new app.ObjectID(req.session.userId)}).then(
                        resp => {
                           if(resp) {
                               app.bcrypt.compare(req.body.confirmation, resp.password).then(
                                   async status => {
                                       if(!status) return(reject({msg: "NOTSAME"}))
                                       else {
                                           let validMimes = ["image/jpeg", "image/bmp", "image/png"], mimeFlag = false
                                           for (let mime of validMimes)
                                               if (req.file.nimetype == mime) mimeFlag = true
                                           if (mimeFlag) return (reject({msg: "WRONGTYPE"}))

                                           if (req.file.size / 1024 / 1024 > 10) return (reject({msg: "WRONGSIZE"}))

                                           await jimp.read(req.file.path, (err, image) => {
                                               if (err) reject({msg: "WRONGPICTURE", err})
                                               else {
                                                   image
                                                       .quality(60)
                                                       .scale(getScale(image.bitmap, 300))

                                                   if (req.user.hasProfilePicture) app.fs.unlinkSync(`public/files/users/${req.user.clientId}/profile_picture.${req.user.profilePictureExt}`)

                                                   let ext = req.file.originalname.split(".")
                                                   image.write(`public/files/users/${req.user.clientId}/profile_picture.${ext[ext.length - 1]}`)

                                                   app.dbClient.db("main").collection("users").updateOne({clientId: req.user.clientId}, {$set: {profilePictureExt: ext[ext.length - 1]}});

                                                   resolve()
                                               }
                                           }).catch(err => reject({msg: "SAVEERROR", err}))
                                       }
                                   },
                                   err => reject({msg: "HASHERROR", err})
                               )
                           } else reject({msg: "CONNERROR", err})
                        },
                        err => reject({msg: "CONNERROR", err})
                    )}
                    else reject({msg: "NOUSER"})
            }).then(
                () => {
                    app.fs.unlinkSync(req.file.path)
                    res.json({err: false})
                },
                err => {
                    app.fs.unlinkSync(req.file.path)
                    let validations = {}

                    switch(err.msg) {
                        case "NOUSER":
                            validations = {err: true}
                            break
                        case "HASHERROR":
                            validations = {err: true}
                            break
                        case "SAVEERROR":
                            validations = {err: true}
                            break
                        case "WRONGPICTURE":
                            validations = {err: true, validations: {profilePicture: err.msg}}
                            break
                        case "WRONGSIZE":
                            validations = {err: true, validations: {profilePicture: err.msg}}
                            break
                        case "WRONGTYPE":
                            validations = {err: true, validations: {profilePicture: err.msg}}
                            break
                        case "NOTSAME":
                            validations = {err: true, validations: {confirmation: err.msg}}
                            break
                        case "CONNERROR":
                            validations = {err: true, validations: {confirmation: "CONNECTIONERROR"}}
                            break
                    }
                    res.json(validations)
                }
            )
        }
        else res.json({err: true})
    })

    server.post("/changeUserData", app.upload.fields([]), (req, res) => {
        return new Promise((resolve, reject) => {
            inputValidation(app, req).then(
                async () => {
                    let objectToSet = {$set: {}}

                    for (let field in req.body) objectToSet["$set"][field] = req.body[field]
                    delete objectToSet["$set"].confirmation

                    if (objectToSet["$set"].password) objectToSet["$set"].password = await app.bcrypt.hash(objectToSet["$set"].password, 10)

                    app.dbClient.db("main").collection("users").updateOne({"_id": new app.ObjectID(req.session.userId)}, objectToSet).then(
                        () => resolve(),
                        err => reject({msg: "CONNECTIONERROR", err})
                    )
                },
                err => reject(err)
            )
        }).then(
            () => res.send({err: false}),
            (err) => {
                console.log(err)
                switch (err.msg) {
                    case "CONNECTIONERROR":
                        res.send({err: true})
                        break
                    case "INPUTERROR":
                        res.send({err: true, validations: err.err})
                        break
                    case "HASHERROR":
                        res.send({err: true})
                        break
                    case "NOUSER":
                        res.send({err: true})
                        break
                }
            }
        )
    })
}