const inputValidation = require("../collectors/inputValidation")
module.exports = (server, app) => {
    server.post("/updates-rulesAccept", app.upload.fields([]), (req, res) => {
        return new Promise((resolve, reject) => {
            let birthDate = req.body.day + "-" + req.body.month + "-" + req.body.year
            inputValidation(app, {body:{ date: [req.body.day, req.body.month, req.body.year], lang: req.body.lang, country: req.body.country}}).then(
                () => {
                    app.dbClient.db("main").collection("users").updateOne({_id: new app.ObjectID(req.session.userId)}, {$set: {termsAccepted: true, birthDate, country: req.body.country, prefLang: req.body.lang}}).then(
                        () => {
                            req.user.birthDate = birthDate
                            req.user.country = req.body.country
                            req.user.prefLang = req.body.lang
                            req.user.termsAccepted = true
                            resolve()
                        },
                        () => reject()
                    )
                },
                (err) => reject(err.err)
            )
        }).then(
            () => res.send({err: false}),
            (validations) => res.send({err: true, validations})
        )
    })
}