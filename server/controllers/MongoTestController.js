module.exports = (server, app) => {
    server.post('/mongoTest1', (req, res) => {
        /*app.dbClient.db("main").collection("questions").updateOne({id: "qstScheme", "tree.casual": "20005"}, {$set: {
                "tree.casual.$": NULL
            }})*/
        /*app.dbClient.db("main").collection("questions").updateOne({id: "qstScheme"}, {$pull: {
                "tree.casual": "30000"
            }})*/
        app.dbClient.db("main").collection("questions").updateOne({id: "qstScheme"}, {$unset: {
                "tree.precise.10004": ""
            }})
            .then(() => {
                res.send({data: "zaktualizowano"})
            },
                err => {
                    console.log(err)
                    res.send({data: "Fatal"})
            })
    })
    server.post('/mongoTest2', (req, res) => {
        app.dbClient.db("main").collection("questions").updateOne({id: "qstScheme", "tree.casual": "20005"}, {$set: {
                "tree.casual.$": "30000"
            }})
            .then(() => {
                    res.send({data: "zaktualizowano"})
                },
                err => {
                    console.log(err)
                    res.send({data: "Fatal"})
                })
    })
}