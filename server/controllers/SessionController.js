module.exports = (server) => {
    server.post("/sessionControl", async (req, res) => {
        return new Promise((resolve, reject) => {
            if(req.session) {
                switch(req.body.operation){
                    case "delete":
                        req.session.destroy((err) => {
                           if(err) reject({err: true, msg: err})
                           else resolve()
                        })
                        break
                    case "regenerate":
                        req.session.regenerate((err) => {
                            if(err) reject({err: true, msg: err})
                            else resolve()
                        })
                        break
                    default:
                    reject({err: true, msg: "NOOPARATION"})
                    break
                }
            }
            else reject({err: true, msg: "NOSESSION"})
        }).then(
            () => {
                res.send({err: false})
            },
            (err) => {
                //Need to log err.msg somewhere
                res.send({err: true})
            }
        )
    })
}