var defaultLanguage, checkBrowserLanguage = true

/*Determine and return the most optimal language code for the current user*/

function getPreferredLang(req){
    let userLanguage, headers

    //we need to deliver headers and cookies because withContributions option in axios does not work properly
    //So req.body.* way is for /langPack route and req.* for regular routing

    //Analyzing headers. If the user is new or doesn't have lang cookie we try to get his browsers language
    if(req.body.headers) headers = req.body.headers
    else if(req.headers) headers = req.headers
    if(headers["accept-language"]) userLanguage = headers["accept-language"].split(";")[0].split(",")[0].split("-")[0].trim()
    //If user not new it means he has a lang cookie in his browser's storage. We use this info then in first
    let cookieLanguage
    if(!req.cookies.lang)
        if(req.body.cookies) cookieLanguage = req.body.cookies.lang
    else cookieLanguage = req.cookies.lang

    //If both methods failed we set a static, fully supported default lang
    let preferredLang = defaultLanguage

         if (cookieLanguage) preferredLang = cookieLanguage
    else if (userLanguage && userLanguage != "" && checkBrowserLanguage) preferredLang = userLanguage
    return preferredLang
}

module.exports = (server, app) => {

    for(let language of app.languages)
        if(language.default) defaultLanguage = language.code

    server.get("/", (req, res) => {
        return res.redirect(`/${getPreferredLang(req)}`)
    })

    server.get("/:lang", (req, res) => {
        return app.render(req, res, "/")
    })

    /*We try to get proper lang pack combinations, joining together both preferred lang and default lang*/
    server.post("/langPack", (req, res) => {
        let prefLang = getPreferredLang(req)

        if(req.body.lang == prefLang) {
            let full = {langPack: {}, serverData: {}}
            let langPack = {main: {}, default: {}}
            try {
                langPack.main = require(`../langPacks/${prefLang}.js`)
                langPack.default = require(`../langPacks/${defaultLanguage}.js`)

                if (defaultLanguage !== req.body.lang) langPack.default = {...langPack.default, ...langPack.main}
                full.langPack = langPack.default
                //send safe client data from a module (like key for captcha)
                full.serverData = app.clientData
                //set cookie lang (used to set lang cookie on client when it's not set already)
                full.cookieLang = prefLang
            }
            catch (err) {
                //if an error appeared while getting lang pack we handle it
                app.errorLog.error()
                return (res.send({error: "E1000-1"}))
            }
            return res.send(full)
        }
        else res.send({prefLang})
    })

    server.post('/forceLangPack', (req, res) => {
        try {
            let prefLang = getPreferredLang(req)
            let main = require(`../langPacks/${prefLang}.js`)
            let def = require(`../langPacks/${defaultLanguage}.js`)
            main = {...def, ...main}

            res.send({err: false, data: main})
        }
        catch(err) {
            return res.send({err: true})
        }
    })

    server.post('/functionLangPack', (req, res) => {
        try {
            let prefLang = req.body.lang
            let main = require(`../langPacks/${prefLang}F.js`)
            for(let i=1; ; i++)
                if(!main[req.body.name + "_" + i])
                    if(i == 1) {
                        if(main[req.body.name]) {
                            return res.send({err: false, data: eval(main[req.body.name])(...req.body.parameters)})
                        }
                        else {
                            let def = require(`../langPacks/${defaultLanguage}F.js`)
                            for(let j=1; ; j++)
                                if(!def[req.body.name + "_" + j])
                                    if(j == 1) {
                                        return res.send({err: false, data: eval(def[req.body.name])(...req.body.parameters)})
                                    }
                                    else {
                                        let max = j - 0.00000000001, min = 1
                                        let chosenOne = Math.floor(Math.random() * (+max - +min) + +min)
                                        return res.send({err: false, data: eval(def[req.body.name + "_" + chosenOne])(...req.body.parameters)})
                                    }
                        }
                    }
                    else {
                        let max = i - 0.00000000001, min = 1
                        let chosenOne = Math.floor(Math.random() * (+max - +min) + +min)
                        return res.send({err: false, data: eval(main[req.body.name + "_" + chosenOne])(...req.body.parameters)})
                    }
        }
        catch(err) {
            return res.send({err: true})
        }
    })
}