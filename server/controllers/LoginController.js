const Validators = require("../collectors/Validators")

module.exports = (server, app) => {
    server.post("/login", app.upload.fields([]), async (req, res) => {
        req.body.email = req.body.email.trim().toLowerCase()
        req.body.password = req.body.password

        return new Promise(async (resolveMain, rejectMain) => {
            for(let field in req.body){
                if(req.body.asdfzxcv) break
                switch(field){
                    case "email":
                        if (Validators[field](req.body[field])) return (rejectMain({msg: "WRONGEMAILORPASSWORD"}))
                        break
                    case "password":
                        if (Validators[field](req.body[field])) return (rejectMain({msg: "WRONGEMAILORPASSWORD"}))
                        break
                    case "token":
                        if (await new Promise((resolve) => {
                            app.request.post({
                                    url: "https://www.google.com/recaptcha/api/siteverify",
                                    form: {
                                        secret: app.serverData.secretKey,
                                        response: req.body.token,
                                        remoteip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
                                    }},
                                (err, response, body) => {
                                    if(err) return(rejectMain({msg: "CONNECTIONERROR", err}))

                                    if(!JSON.parse(body).success) resolve("UNPASSEDCAPTCHA")
                                    else resolve(false)
                                })
                        })) return (rejectMain({msg: "UNPASSEDCAPTCHA"}))
                        break
                }
            }
            /*Check if user exists in database*/
            app.dbClient.db("main").collection("users").findOne({email: req.body.email}).then(
                (response) => {
                    if(response)
                        app.bcrypt.compare(req.body.password, response.password).then(
                            (response2) => {
                                if(response2){
                                    app.dbClient.db("main").collection("users").updateOne({email: req.body.email}, {$inc: {loginNumber: 1}}).then(
                                        () => {
                                            req.session.userId = response["_id"].toHexString()
                                            req.session.save()
                                            resolveMain()
                                        },
                                        () => rejectMain({msg: "CONNECTIONERROR"})
                                    )
                                }
                                else{
                                    rejectMain({msg: "WRONGEMAILORPASSWORD"})
                                }
                            },
                            () => rejectMain({msg: "CONNECTIONERROR", err})
                        )
                    else rejectMain({msg: "WRONGEMAILORPASSWORD"})
                },
                (err) => rejectMain({msg: "CONNECTIONERROR", err})
            )
        }).then(
            () => {
                res.send({err: false})
            },
            (err) => {
                let msg = err.msg
                switch(err.msg){
                    case "CONNECTIONERROR":
                        msg = "E1004-1"
                        break
                }
                res.send({err: true, msg})
            }
        )
    })
}