module.exports = (server, app) => {
    server.post("/logMe", (req, res) => {
        try{
            let logger = app[req.body.varName]
            logger[req.body.type](req.body.message)
        }
        catch(err){
            app.errorLog.warn(`1001 - Some logs did not save: name = ${req.body.varName};  type = ${req.body.type}; msg = ${req.body.message}`)
            res.error()
        }
    })
}