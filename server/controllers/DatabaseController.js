const Validators = require("../collectors/Validators")

/*Update has been commented due to finding more vulnerable cases than expected (security reasons)*/


/*Header check function for getting absolute permissions*/

function checkHeader(userRank, connection, db, collection, op) {
    return new Promise((resolve, reject) => {
        let permissions = {
            read: false,
            write: false
        }

        //Get collection's header from database
        connection.db(db).collection(collection).findOne({id: "header"}).then(
            (res) => {
                if (res) {
                    let neededPerms = {
                        read: false,
                        write: false
                    }
                    //Find out what permissions actually we need
                    if (op != "insertOne" && op != "insertMany") neededPerms.read = true
                    if (op != "findOne" && op != "find") neededPerms.write = true

                    //Get read/write roles stored in header (in "allowed" object), if * found - admit permissions
                    if (neededPerms.read) {
                        if (res.allowed) {
                            for (let rank of res.allowed.read) {
                                if (rank == "*") permissions.read = true
                                if (rank == userRank) permissions.read = true
                            }
                        }
                    }
                    if (neededPerms.write) {
                        if (res.allowed) {
                            for (let rank of res.allowed.write) {
                                if (rank == "*") permissions.write = true
                                if (rank == userRank) permissions.write = true
                            }
                        }
                    }
                    resolve(permissions)
                }
                else resolve(null)
            },
            (err) => reject({msg: "NOCONNECTION", err})
        )
    })
}

/*Check access to a document by looking inside "allowed" and comparing its info with our's client data*/

function checkAccess(document, userRank, userId, type) {
    if (type == "findOne") type = "find"
    else if (type == "deleteOne" || type == "deleteMany") type = "delete"
    /*else if (type == "updateOne" || type == "updateMany") type = "update"*/

    if(document.allowed) {
        if (type == "find"/* || type == "update"*/) {
            //Check all of the cases kept in "allowed" to confirm access to the following document
            //return hasaccess... flag to tell further parts of script if user had access to fields stored in "fields" or not
            for (let rank in document.allowed[type][0])
                if (rank == userRank) return ({hasAccessToFollowing: false, fields: document.allowed[type][0][rank]})
            for (let id in document.allowed[type][1])
                if (id == userId) return ({hasAccessToFollowing: false, fields: document.allowed[type][1][id]})
            if (userId) return ({hasAccessToFollowing: true, fields: document.allowed[type][2]})
            else return ({hasAccessToFollowing: true, fields: document.allowed[type][3]})
        }
        else if (type == "delete") {
            //In "delete" we actually don't need to know if user could delete separate fields but whole document
            for (let rank of document.allowed.delete[0]) if (rank == userRank) return (true)
            for (let id of document.allowed.delete[1]) if (id == userId) return (true)
            return (false)
        }
    }
}

/*Finds required documents first, checks if any of them is not allowed to user and rejects if any was*/

function checkConnectionOfMany(userRank, userId, connection, db, collection, query, operation, options) {
    return new Promise((resolve, reject) => {

        //For "find" operation we are allowed to provide "options" to tell us that we want to limit or sort documents
        let findOp = connection.db(db).collection(collection).find(query)

        if(options) {
            if (options.sort && operation == "find") findOp = findOp.sort(options.sort)
            if (options.limit && operation == "find") findOp = findOp.limit(options.limit)
        }

        //Find document that we'll be operating on
        findOp.toArray().then(
            (documents) => {
                let final = []

                for (let document of documents) {
                    //We don't want to operate on header in any case
                    if(document.id == "header") continue
                    else if (document.allowed) {
                        let access = checkAccess(document, userRank, userId, operation)

                        let validObject

                        /*if (operation == "updateMany" && !Aflag) {
                            access.hasAccessToFollowing ? final = {$set: {}} : final = options
                            delete final["$set"]["_id"]
                            delete final["$set"].allowed

                            Aflag = true
                        }*/

                        //for find we need to exclude vulnerable fields
                        if (operation == "find") {
                            access.hasAccessToFollowing ? validObject = {} : validObject = document
                            delete validObject["_id"]
                            delete validObject.allowed
                        }

                        switch (operation) {
                            case "find":
                                //Depends on the hasAccess... flag we need to either exclude unwanted fields or include those wanted
                                for (let field of access.fields)
                                    for (let docField in document)
                                        if (access.hasAccessToFollowing && docField == field)
                                            validObject[field] = document[docField]
                                        else if (!access.hasAccessToFollowing && docField == field) {
                                            delete validObject[field]
                                        }
                                final.push(validObject)
                                break
                            /*case "updateMany":
                                for (let field of access.fields) {
                                    for (let docField in options["$set"]) {
                                        if (access.hasAccessToFollowing && docField == field) {
                                            if (field != "") final["$set"][field] = options["$set"][field]
                                        }
                                        else if (!access.hasAccessToFollowing && docField == field) {
                                            delete final["$set"][field]
                                        }
                                    }
                                }
                                break*/
                            case "deleteMany":
                                if (!access) return (reject({msg: "NOACCESS"}))
                                break
                        }
                    }
                    else return(reject({msg: "NOALLOWEDSECTION"}))
                }

                switch (operation) {
                    case "find":
                        resolve(final)
                        break
                   /* case "updateMany":
                        connection.db(db).collection(collection).updateMany(query, final).then(
                            () => resolve(),
                            (err) => reject({msg: "NOCONNECTION", err})
                        )
                        break*/
                    case "deleteMany":
                        connection.db(db).collection(collection).deleteMany(query).then(
                            () => resolve(),
                            (err) => reject({msg: "NOCONNECTION", err})
                        )
                        break
                }
            },
            (err) => reject({msg: "NOCONNECTION", err})
        )
    })
}

/*For this function we have the same story as with "many" but we operate on one document so we just don't need to
* collect all of the sanitized documents before doing actual operation*/

function checkConnectionOfOne(userRank, userId, connection, db, collection, query, operation, options) {
    return new Promise((resolve, reject) => {
        connection.db(db).collection(collection).findOne(query).then(
            (document) => {
                if(document) {
                    if(document.id == "header") return(resolve())
                    if (document.allowed) {
                        let access = checkAccess(document, userRank, userId, operation)

                        let validObject = options

                        /*if (operation == "updateOne") {
                            access.hasAccessToFollowing ? validObject = {$set: {}} : validObject = options
                            delete validObject["$set"]["_id"]
                            delete validObject["$set"].allowed
                        }*/

                        if (operation == "findOne") {
                            access.hasAccessToFollowing ? validObject = {} : validObject = document
                            delete validObject["_id"]
                            delete validObject.allowed
                        }

                        switch (operation) {
                            case "findOne":
                                for (let field of access.fields)
                                    for (let docField in document)
                                        if (access.hasAccessToFollowing && docField == field)
                                            validObject[field] = document[docField]
                                        else if (!access.hasAccessToFollowing && docField == field) {
                                            delete validObject[field]
                                        }
                                return resolve(validObject)
                                break
                            /*case "updateOne":
                                for (let field of access.fields) {
                                    for (let docField in options["$set"]) {
                                        if (access.hasAccessToFollowing && docField == field) {
                                            if (field != "") validObject["$set"][field] = options["$set"][field]
                                        }
                                        else if (!access.hasAccessToFollowing && docField == field) {
                                            delete validObject["$set"][field]
                                        }
                                    }
                                }

                                connection.db(db).collection(collection).updateOne(query, validObject).then(
                                    () => resolve(),
                                    (err) => reject({msg: "NOCONNECTION", err})
                                )
                                break*/
                            case "deleteOne":
                                if (!access) return (reject({msg: "NOACCESS"}))
                                if (query.id == "header") {
                                    delete query.id
                                }
                                else connection.db(db).collection(collection).deleteOne(query).then(
                                    () => resolve(),
                                    (err) => reject({msg: "NOCONNECTION", err})
                                )
                                break
                        }
                    }
                }
                else
                    switch(operation){
                        case "findOne":
                            resolve(null)
                            break
                        default:
                            reject({msg: "NOTHINGFOUND"})
                    }
            },
            (err) => reject({msg: "NOCONNECTION", err})
        )
    })
}

module.exports = (server, app) => {
    server.post("/clientDb", (req, res) => {
        return new Promise(async (resolve, reject) => {
            let rank, userId, op = req.body.operation

            //Check if we are either dealing with client or server request
            //For client one we need to check if connection strings were safe
            //For server we can take it as granted and give admin privileges
            let reqType = req.headers.origin || req.headers.referer ? "client" : "server"
            if(reqType == "client" && req.session.userId) {
                await app.dbClient.db("main").collection("users").findOne({"_id": new app.ObjectID(req.session.userId)}).then(
                    (resp) => {
                        userId = resp["_id"].hex
                        rank = resp.type
                    },
                    (err) => reject({msg: "NOCONNECTION"}, err)
                )
            }
            else if(reqType == "server") {
                userId = "server"
                rank = "admin"
            }
            else {
                rank = undefined
                userId = undefined
            }

            //We need to check if all required parts of connection data were provided
            if (!req.body.queryData || typeof req.body.queryData != "object") return (reject({msg: "NOQUERY"}))
            if(req.body.queryData.id == "header")  return (reject("NOACCESS"))

            /*if (op == "updateMany" || op == "updateOne") {
                if(!req.body.options || typeof req.body.options != "object") return (reject({msg: "NOOPTIONS"}))
                if(req.body.options["$set"].allowed) return (reject("NOACCESS"))
            }*/

            //Just simply checks header. If user fits in its allowance, he gains a full access to document's data (admin). If not data is sanitized
            checkHeader(rank, app.dbClient, req.body.db, req.body.collection, op).then(
                (resp) => {
                    if (!resp) resp = {read: false, write: false}

                    if (op == "insertMany" || op == "insertOne") {
                        if (resp.write) {
                            return app.dbClient.db(req.body.db).collection(req.body.collection)[op](req.body.queryData).then(
                                () => resolve(true),
                                (err) => reject({msg: "NOCONNECTION", err})
                            )
                        }
                        else reject({msg: "NOACCESS"})
                    }
                    else if(op == "find" || op == "findOne") {
                        if(resp.read) {
                            if(op == "find") {
                                return app.dbClient.db(req.body.db).collection(req.body.collection).find(req.body.queryData, req.body.options).toArray().then(
                                    (resp) => resolve(resp),
                                    (err) => reject({msg: "NOCONNECTION"}, err)
                                )
                            }
                            else if(op == "findOne") {
                                return app.dbClient.db(req.body.db).collection(req.body.collection).findOne(req.body.queryData).then(
                                    (resp) => resolve(resp),
                                    (err) => reject({msg: "NOCONNECTION"}, err)
                                )
                            }
                        }
                        else if (op == "find") {
                            return checkConnectionOfMany(rank, userId, app.dbClient, req.body.db, req.body.collection, req.body.queryData, op, req.query.options).then(
                                (response) => resolve(response),
                                (err) => reject({msg: "NOCONNECTION", err})
                            )
                        }
                        else if (op == "findOne") {
                            return checkConnectionOfOne(rank, userId, app.dbClient, req.body.db, req.body.collection, req.body.queryData, op).then(
                                (response) => resolve(response),
                                (err) => reject({msg: "NOCONNECTION", err}))
                        }
                    }
                    else {
                        if (resp.read && resp.write) {
                            return app.dbClient.db(req.body.db).collection(req.body.collection)[op](req.body.queryData, req.body.options).then(
                                () => resolve(true),
                                (err) => reject({msg: "NOCONNECTION", err})
                            )
                        }
                        else {
                            if (op == "deleteMany"/* || op == "updateMany"*/)
                                return checkConnectionOfMany(rank, userId, app.dbClient, req.body.db, req.body.collection, req.body.queryData, op/*, req.body.options*/).then(
                                    () => resolve(),
                                    (err) => reject(err)
                                )
                            if (op == "deleteOne"/* || op == "updateOne"*/)
                                return checkConnectionOfOne(rank, userId, app.dbClient, req.body.db, req.body.collection, req.body.queryData, op/*, req.body.options*/).then(
                                    () => resolve(),
                                    (err) => reject(err)
                                )
                            else reject({msg: "NOCONNECTION", err})
                        }
                    }
                },
                (err) => reject(err)
            )

        }).then(
            (resp) => {
                return res.send({err: false, response: resp})
            },
            (err) => {
                let errMsg;
                err.msg ? errMsg = err.msg : errMsg = err
                //TODO: Need to log err.err
                return res.send({err: true})
            }
        )
    })
}