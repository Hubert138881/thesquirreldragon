const Validators = require("../collectors/Validators")
const typeChecker = require("../collectors/TypeCheckers")
const QuestionsFunctions = require("../collectors/questionFunctions")

module.exports = (server, app) => {
    server.post("/console", app.upload.fields([]), (req, res) => {
        let questionFunctions = new QuestionsFunctions(app, req)
        
        let qDB = app.dbClient.db("main").collection("questions"), 
        uDB = app.dbClient.db("main").collection("users")
        let s = req.body.command

        s = s.split("##").map(one => {
            return [one, "##"]
          }).flat()
          s.splice(s.length - 1)
          
          let params = []
          for(let i = 0; i < s.length; i++)
            if(s[i] == "##" && s[i+2] == "##") {
                params.push([s[i+1]])
              i+=2
            }
            else params.push(s[i])
            params = params.map(one => {
                if(typeof one == "string") 
                    return one.trim().split(" ")
                return [one[0].trim()]
            }).flat().filter(one => one != "")
        
        return new Promise((resolve, reject) => {
            /**
             * Possible commands:
                * Questions:
                * - addcat short_name full_name description
                * - addsub 
                * - delsub short_sub_name
             */
            
            let comTexts = {
                addcat: "addcat\tshort_name\tfull_name\tdescription\t\t- add a new category",
                addsub: "addsub\tshort_name\tfull_name\towners_subcategory\tquestion\t- add new subcategory",
                delsub: "delsub\tsub_name\tflag\n\t-c Leave answers\n\t-d Delete literally everything connected with this sub",
                listcat: "listcat\n\t-s (subcategories only)\n\t-a categories and subcategories together",
            }

            if(params[1] == "-h") {
                if(!comTexts[params[0]])
                   return reject(`No help available for command ${params[0]}`)
                else
                   return resolve(comTexts[params[0]])
            }
            switch(params[0]) {
                case "help":
                    let maxPages = Math.ceil(Object.keys(comTexts).length / 10)

                    let helpText = `Page ${params[1]} of ${maxPages}\n`
                    if(!params[1]) {
                        resolve(
`Hello, this is a help command.
All the parameters that contain spaces need to be opened and closed with ##
Type "help 1" to go to the 1 of ${maxPages} pages.`
                        )
                    }
                    else {
                        let page = parseInt(params[1])
                        if(page <= maxPages && page > 0) {
                            for(let i=page*10-10; i<page*10; i++){
                                if(!Object.keys(comTexts)[i]) {
                                    return resolve(helpText)
                                }
                                else {
                                    let comText = comTexts[Object.keys(comTexts)[i]]
                                    helpText += `${comText}\n`
                                }
                            }
                            return resolve(helpText)
                        }
                        else reject(`Maximal page is ${maxPages}`)
                    }
                    break
                case "addcat":
                    let cat
                    typeChecker(app, req, "admin").then(async () => {
                        if(!params[1] || !params[2] || !params[3]) 
                            resolve("Addcat is ready: \n"+comTexts["addcat"])
                        cat = params[1].trim()
                        //Check if such sub does not already exist and if cat does
                        return await qDB.findOne({$or: [
                            {id: "categories", [`categories.${cat}`]: {$exists: true}},
                            {id: "subcategories", [`subcategories.${cat}`]: {$exists: true}},
                            {$and: [
                                {id: "categories", [`categories.${cat}`]: {$exists: false}},
                                {id: "subcategories", [`subcategories.${cat}`]: {$exists: false}}
                            ]}
                        ]})
                    }).then((resp) => {
                        if(resp) {throw {s: "That name is already occupied"}}
                        return Promise.all([
                            qDB.updateOne({id: "categories"}, {$set: {[`categories.${cat}`]: {
                                name: params[2],
                                description: params[3]
                            }}}),
                            qDB.updateOne({id: "scheme"}, {$set: {[`scheme.${cat}`]: []}}),
                        ])
                    }).then(() => resolve("New category has been added to database"))
                    .catch(err => err.s ? reject(err.s) : reject("Some unexpected error occured"))
                    break
                case "listcat":
                    typeChecker(app, req, "moderator").then(() => {
                        return Promise.all([
                            qDB.findOne({id: "categories"}),
                            qDB.findOne({id: "subcategories"})
                        ])
                    }).then(resp => {
                        let textToSend = ""
                        if(!params[1] || params[1] == "-a") {
                            textToSend += "categories: \n"
                            Object.keys(resp[0].categories).map(short => {
                                textToSend += `${short}\t${resp[0].categories[short].name}\n`
                            })
                        }
                        if(params[1] == "-s" || params[1] == "-a") {
                            textToSend += "subcategories: \n"
                            Object.keys(resp[1].subcategories).map(short => {
                                textToSend += `${short}\t${resp[1].subcategories[short].name}\n`
                            })
                        }
                        resolve(textToSend)
                    })
                    .catch(err => err.s ? reject(err.s) : reject("Some unexpected error occured"))
                    break
                case "addsub":
                    if(!params[1] || !params[2] || !params[3] || !params[4]) 
                    return resolve("Addsub is ready: \n"+comTexts["addsub"])
                    questionFunctions
                    .addSubcategory(params[4], params[1], params[2], params[3], qDB)
                    .then(
                        () => resolve("New subcategory has been successfully added"),
                        err => {
                            console.log(err)
                            switch(err.msg) {
                                case "WRONG_QUESTION":
                                    reject("Something is wrond with your question")
                                    break
                                case "SUB_EXISTS":
                                    reject("That subcategory already exists")
                                    break
                                case "NO_MATCHING_CAT_OR_SUB":
                                    reject("Your subcategory is supposed to belong to unexisting subcategory")
                                    break
                                default:
                                    reject("Some unexpected error occured")
                            }
                        }
                    )
                    break
                case "delsub":
                    let old_scheme
                    if(!params[1] || !params[2]) 
                    return resolve("Delsub is ready: \n"+comTexts["addsub"])
                    Validators.subcategory([params[2]]).then(resp => {
                        if(params[1] == "-a" || params[1] == "-b" || params[1] == "-c") {
                            return qDB.findOne({id: "scheme"})
                        }
                        else throw `There is no option "${params[1]}"`
                    }).then(scheme => {
                        old_scheme = scheme.scheme
                        return Promise.all([
                            qDB.updateOne({id: "subcategories"}, {$unset: {[`subcategories.${params[2]}`]: ""}}),
                            qDB.updateOne({id: "scheme"}, {$unset: {[`scheme.${params[2]}`]: ""}}),
                            qdb.deleteOne({_id: params[2]})
                        ])
                    }).then(resp => {
                        if(params[2] == "-d") 
                            return qDB.deleteMany({_id: {$in: old_scheme[params[2]]}})
                        else {
                            return Validators.subcategory([params[2]]).then(resp => {
                                Promise.all([
                                    qDB.updateOne({id}),
                                ])
                            })
                        }
                    })
                    .catch(err => {})
                    break 
                default:
                    if(!params[0]) resolve()
                    else reject(`No such command!`)
            }
        }).then(
            msg => {
                res.json({msg})
            },
            err => {
                console.log(err)
                res.json({err: true, msg: `${params[0]}:\t${err}`})
            }
        )
    })
}