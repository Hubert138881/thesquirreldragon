import React, {useState, useEffect} from 'react'
import Database from "../../components/collectors/DatabaseCollector"

function getUsers(props){
    let usersReady = []

    for(let user of props.predefined.userData) {

        usersReady.push(<tr>
            <td>
                <a href={`/${props.query.lang}/profile/${user.clientId}`}>{user.clientId}</a>
            </td>
            <td>{user.email}</td>
            <td>{user.username}</td>
            <td>{user.type}</td>
        </tr>)
    }
    return usersReady
}

const userList = (props) => {
    return (
        <table>
            <tbody>
                <style>{"\
                    td{\
                      padding-right: 30px;\
                    }\
                  "}</style>
                <tr key={props.query.clientId}><td>{props.langPack["client-id"]}</td><td>{props.langPack["email"]}</td><td>{props.langPack["username"]}</td><td>{props.langPack["account-type"]}</td></tr>
                {getUsers(props)}
            </tbody>
        </table>
    )
}

userList.getInitialProps = async (props) => {
    return await new Promise((resolve) => {
        Database.find("main", "users", {}).then(
            (data) => {
                resolve({err: false, userData: data})
            },
            () => {
                resolve({err: true})
            }
        )
    })
}

export default userList