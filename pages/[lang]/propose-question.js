import entranceLayout from '../../components/layouts/entranceLayout'
import ProposeQuestion from '../../components/elements/proposeQuestion'
const proposeQuestion = (props) => {
    if(props.userData.termsAccepted && props.userData.categories[0]){
        return (
            <>
                {entranceLayout(props,
                    () => <>
                    
                    </>,
                    () => <>
                        <ProposeQuestion {...props}/>
                    </>,
                    () => <></>
                )}
            </>)
    }
    useEffect(() => location.href = `/${props.query.lang}`, [])
    return <></>
 }
 export default proposeQuestion