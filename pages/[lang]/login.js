import React, {useEffect} from "react"
import LoginForm from "../../components/LoginForm"
import Lang from "../../components/collectors/LangCollector";
import Registration from "./registration";

const Login = (props) => {
    if(props.userData.clientId) {
        useEffect(() => location.href = `/${props.query.lang}`, [])
        return <></>
    }
    return (<LoginForm {...props}/>)
}
Login.getInitialProps = async (props) => {
    let langPack = await Lang.forceGetLangPack(props.query.lang, props.req.headers, props.req.cookies)
    return {
        email: await Lang.getFunctionPack("NOTHING", [langPack.email.toLowerCase()], props.query.lang),
        password: await Lang.getFunctionPack("NOTHING", [langPack.password.toLowerCase()], props.query.lang)
    }
}

export default Login