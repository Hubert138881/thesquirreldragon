import React, {useEffect} from "react"
import RegistrationForm from "../../components/RegistrationForm"
import Lang from "../../components/collectors/LangCollector";
import Login from "./login";

const Registration = (props) => {
    if(props.userData.clientId) {
        useEffect(() => location.href = `/${props.query.lang}`, [])
        return <></>
    }
    else return (<RegistrationForm {...props}/>)

}

Registration.getInitialProps = async (props) => {
    let langPack = await Lang.forceGetLangPack(props.query.lang, props.req.headers, props.req.cookies)
    return {
        username:  await Lang.getFunctionPack("NOTHING", [langPack.username.toLowerCase()], props.query.lang),
        email: await Lang.getFunctionPack("NOTHING", [langPack.email.toLowerCase()], props.query.lang),
        password: await Lang.getFunctionPack("NOTHING", [langPack.password.toLowerCase()], props.query.lang)
    }
}

export default Registration