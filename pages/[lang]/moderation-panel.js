import React, {useEffect, useState} from 'react'
import entranceLayout from '../../components/layouts/entranceLayout'
import ChangeCategories from '../../components/elements/changeCategories'
import AddSubcategory from '../../components/elements/AddSubcategory'
import ProposeQuestions from '../../components/elements/proposeQuestion'
import AcceptQuestions from '../../components/elements/acceptQuestions'
import AddQuestion from '../../components/elements/addQuestion'
import GetQuestion from "../../components/elements/getQuestion.js"
import ModifyQuestion from "../../components/elements/modifyQuestion"
import ManageFlagged from '../../components/elements/manageFlagged'
import AddTranslations from '../../components/elements/addTranslations'

import TypeCheckers from '../../server/collectors/TypeCheckers'

const qstconpnl = (props) => {
    const [cathState, setCathState] = useState(true)
    
    if(props.userData.termsAccepted && TypeCheckers(null, null, "moderator", props.userData.type)){
        return (
            <>
                {entranceLayout(props,
                    () => <>
                    {/*<a href={`/${props.query.lang}/search-people`}><button>Look for others</button></a>
                        <h5 style={{fontSize: "25px"}}>Propose question</h5>
                        <ProposeQuestions {...props}/>
                        <h5 style={{fontSize: "25px"}}>Accept question</h5>
                        <AcceptQuestion {...props}/>
                        <h5 style={{fontSize: "25px"}}>Add question</h5>
                        <AddQuestion {...props}/>
                        <h5 style={{fontSize: "25px"}}>Get question</h5>
                        <GetQuestions {...props}/>
                        <h5 style={{fontSize: "25px"}}>Modify question</h5>
                        <ModifyQuestion {...props}/>
                */}
                    {
                        props.userData.categories.length == 0 && cathState ? <>
                            <ChangeCategories {...props} changed={() => {
                                alert("Your new preferences have been saved!")
                                setCathState(false)
                            }}/>
                        </> : <>
                            <p style={{fontSize: "20px"}}>Add Translations (mod)</p>
                            <AddTranslations {...props} success={() => {
                                alert("New category has been added")
                            }} error={err => {
                                console.log(err)
                            }}/> 

                            <p style={{fontSize: "20px"}}>Manage flagged (mod)</p>
                            <ManageFlagged {...props} success={() => {
                                alert("New category has been added")
                            }} error={err => {
                                console.log(err)
                            }}/>

                            <p style={{fontSize: "20px"}}>Add new subcategory (mod)</p>
                            <AddSubcategory {...props} success={() => {
                                alert("New category has been added")
                            }} error={err => {
                                console.log(err)
                            }}/>

                            <p style={{fontSize: "20px"}}>Propose a question</p>
                            <ProposeQuestions {...props} success={() => {
                                alert("New proposition has been added")
                            }} error={err => {
                                console.log(err)
                            }}/>

                            <p style={{fontSize: "20px"}}>Accept proposition (mod)</p>
                            <AcceptQuestions {...props} success={() => {
                                alert("New proposition has been added")
                            }} error={err => {
                                console.log(err)
                            }}/>

                            <p style={{fontSize: "20px"}}>Get and answer question</p>
                            <GetQuestion {...props}/>

                            <p style={{fontSize: "20px"}}>Add new question (mod)</p>
                            <AddQuestion {...props}/>

                            <p style={{fontSize: "20px"}}>Modify questions (mod)</p>
                            <ModifyQuestion {...props}/>
                        </>
                    }
                    </>,
                    () => <>
                        {(() => {
                            return <>

                            </>
                        })()}
                    </>,
                    () => <></>
                )}
            </>)
    }
    useEffect(() => location.href = `/${props.query.lang}`, [])
    return <></>
}
export default qstconpnl