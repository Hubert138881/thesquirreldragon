import React, {useEffect} from "react"
import Database from "../../../components/collectors/DatabaseCollector"

const showProfile = (props) => {
    if(props.predefined.err) return(<div>{"Something went wrong. Please, try to refresh this page"}</div>)
    else if(props.predefined.userData) return(
        <>
            <label>{props.query.clientId}     {props.predefined.userData.type}</label> <br/>
            <img height={250} src={`/files/users/${(() => {
                if(props.predefined.userData.profilePictureExt != "") return(`${props.query.clientId}/profile_picture.${props.predefined.userData.profilePictureExt}`)
                return("default_profile_picture.png")
            })()}`}/> <br/>
            <label>{props.langPack.username}: {props.predefined.userData.username}</label> <br/>
            <label>{props.langPack.email}: {props.predefined.userData.email}</label> <br/>
        </>
    )
    else {
        useEffect(() => (() => location.href = `/${props.query.lang}/userList`)(), [])
        return (<></>)
    }
}

showProfile.getInitialProps = async (props) => {
    return await new Promise((resolve) => {
        Database.findOne("main", "users", {clientId: props.query.clientId}).then(
            (success) => {
                if(success) resolve({err: false, userData: success})
                else resolve({err: false, userData: false})
            },
            () => {
                resolve({err: true})
            }
        )
    })
}

export default showProfile