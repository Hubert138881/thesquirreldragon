import React, {useEffect} from "react"
import PanelsToChangeUserData from "../../components/PanelsToChangeUserData"
import A from "../../components/collectors/DatabaseCollector"
import Lang from "../../components/collectors/LangCollector";
import Login from "./login";
import Registration from "./registration";
import layout from "../../components/layouts/entranceLayout"

const ChangeUserData = (props) => {
    useEffect(() => {
        if(!props.userData["clientId"]) location.href = "login"
    })

    props.query.welcomeBar = true

    if(props.userData["clientId"]) return (
        (() => layout(props, undefined, () => <>
            <PanelsToChangeUserData {...props}/>
        </>))()
    )
    else return (<></>)
}

ChangeUserData.getInitialProps = async (props) => {
    let langPack = await Lang.forceGetLangPack(props.query.lang, props.req.headers, props.req.cookies)
    return {
        username:  await Lang.getFunctionPack("SAME", [langPack.username.toLowerCase()], props.query.lang),
        email: await Lang.getFunctionPack("SAME", [langPack.email.toLowerCase()], props.query.lang),
        password: await Lang.getFunctionPack("NOTHING", [langPack.password.toLowerCase()], props.query.lang),
        confirmation: await Lang.getFunctionPack("NOTHING", [langPack.confirmation.toLowerCase()], props.query.lang)
    }
}

export default ChangeUserData