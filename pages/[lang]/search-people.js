import React, {useEffect} from 'react'
import entranceLayout from '../../components/layouts/entranceLayout'

const SearchPeople = (props) => {
    if(props.userData.termsAccepted)
        return(
            entranceLayout(props,
                () => <>
                    <a href={`/${props.query.lang}/answer-questions`}><button>Answer more questions</button></a>
                </>,
                () => <>
                    {(() => {
                        return <>

                        </>
                    })()}
                </>
            ))
    useEffect(() => location.href = `/${props.query.lang}`, [])
    return <></>
}
export default SearchPeople