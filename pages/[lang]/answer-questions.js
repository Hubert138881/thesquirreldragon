import React, {useEffect, useState} from 'react'
import entranceLayout from '../../components/layouts/entranceLayout'
import ChangeCategories from '../../components/elements/changeCategories'
import GetQuestion from "../../components/elements/getQuestion.js"

const AnswerQuestions = (props) => {
    const [cathState, setCathState] = useState(true)
    if(props.userData.termsAccepted){
        return (
            <>
                {entranceLayout(props,
                    () => <>
                    {
                        props.userData.categories.length == 0 && cathState ? <>
                            <ChangeCategories {...props} changed={() => {
                                alert("Your new preferences have been saved!")
                                setCathState(false)
                            }}/>
                        </> : <>
                            <GetQuestion {...props} noButton={true}/>
                            <a href={`/${props.query.lang}/propose-question`}>Propose your own questions!</a>
                        </>
                    }
                    </>,
                    () => <>
                        {(() => {
                            return <>

                            </>
                        })()}
                    </>,
                    () => <></>
                )}
            </>)
    }
    useEffect(() => location.href = `/${props.query.lang}`, [])
    return <></>
}
export default AnswerQuestions