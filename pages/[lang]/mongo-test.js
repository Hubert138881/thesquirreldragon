import React, {useState} from "react"
import axios from "axios"

const MongoTest = (props) => {
    let serverPath = ""
    if(process.env.npm_package_proxy) serverPath = process.env.npm_package_proxy

    const [click1, setClick1] = useState("")
    const [click2, setClick2] = useState("")

    return(
        <>
            <button onClick = {() => {
                axios({
                    method: 'post',
                    //url: `${serverPath}/mongoTest1`,
                    url: `${serverPath}/questionsProcess`,
                    data: {
                        option: "questionSearch",
                        keyPhrase: "squirrel"
                    }
                }).then(res => res.data).then(
                    res => {
                        if(res.err) {
                            console.log(res.err)
                            setClick1(res.msg)
                        }
                        else {
                            console.log(res.data)
                            setClick1(res.data)
                        }
                    }
                ).catch((err) => {
                    setClick1("Fatal")
                    console.log("Fatal: ", err)
                })
            }}>Click1</button>

            <button onClick = {() => {
                axios({
                    method: 'post',
                    //url: `${serverPath}/mongoTest2`,
                    url: `${serverPath}/questionsProcess`,
                    data: {
                        option: "acceptQuestion",
                        data: {
                            id: "e66fbb35ce3c2fe527b541",
                            type: 1,
                            scheme: {},
                            tree: {
                                "tree.precise.e66fbb35ce3c2fe527b541": {}
                            },
                            translations: {
                                "de": {}
                            }
                        }
                    }
                }).then(res => res.data).then(
                    res => {
                        if(res.err) {
                            console.log(res.err)
                            setClick2(res.msg)
                        }
                        else {
                            console.log(res.data)
                            setClick2(res.data)
                        }
                    }
                ).catch((err) => {
                    setClick2("Fatal")
                    console.log("Fatal: ", err)
                })
            }}>Click2</button>

            <p>Click1: {click1}</p>
            <p>Click2: {click2}</p>
        </>
    )
}
export default MongoTest