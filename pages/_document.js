import Document, {Html, Head, Main, NextScript} from 'next/document'

/*
* Ten plik sluży do konfiguracji szkieletu dokumentu
* Html, Head, Main i NextScript są niezbedne do prawidlowego uruchomienia strony
*/

export default class MyDocument extends Document {
    render() {
        return (
            <Html>
                <Head/>
                <body>
                    <Main/>
                    <NextScript/>
                </body>
            </Html>
        )
    }
}