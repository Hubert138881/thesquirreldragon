import React, {useEffect, useState} from 'react'
import entranceLayout from "../components/layouts/entranceLayout"
import Updates from "../components/collectors/UpdateCollector"
import countries from "../components/utilities/countries.json"
import languages from "../components/utilities/languages.json"

import BirthDateInput from "../components/elements/birthDateSelect"

const Home = (props) => {
    return <>
        {entranceLayout(props,
            () => <h1>TheSquirreldragon</h1>,
            () => <>{(() => {
                let data = []
                const [fatal, fatalSet] = useState(null)

                const [dateStatus, dateStatusSet] = useState(false)
                const [countryStatus, countryStatusSet] = useState("def")
                const [genderStatus, genderStatusSet] = useState("def")
                const [agreement2Status, agreement2StatusSet] = useState(false)

                props.userData.clientId ?
                    props.userData.termsAccepted ?
                        useEffect(() => location.href = `/${props.query.lang}/main-room`, [])
                        : data.push(
                        <>
                            <br/>
                            <form onSubmit={(e) => {
                                e.preventDefault()
                                if (countryStatus && dateStatus)
                                    Updates.rulesAccept(new FormData(e.target)).then(
                                        () => {
                                            alert("Thank you for your patience. Enjoy the site :D")
                                            location.href = `/${props.query.lang}/main-room`
                                        },
                                        (err) => {
                                            if (err) {
                                                for (let input in err) if (err[input]) fatalSet(`Sorry, but something is wrong with ${input} input. Please take care of it!`)
                                            } else fatalSet(`it seems we came across some unexpected error. Please, refresh the page and try once again`)
                                        }
                                    )
                            }}>
                                <p>Hooray. Now to make sure of you, you need to provide some extra data</p>
                                <select name={"country"} onChange={(e) => {
                                    if (e.target.value != "def") countryStatusSet(true)
                                    else countryStatusSet("def")
                                }}>
                                    <option value={"def"}>Select a country you live in</option>
                                    {(() => {
                                        let couElem = []
                                        for (let country of countries) couElem.push(<option
                                            value={country.name}>{country.country}</option>)
                                        return couElem
                                    })()}
                                </select>
                                <p>Select your birth date (You do not need to lie, we only want to protect you)</p>
                                <BirthDateInput {...props} changed={(status) => dateStatusSet(status)}/>
                                <p>Select the language you prefer</p>
                                <select name={"lang"}>
                                    {(() => {
                                        let langOptions = [];
                                        langOptions.push(<option value={props.query.lang}>The one is set right
                                            now</option>)
                                        languages.map(lang => langOptions.push(<option
                                            value={lang.code}>{lang.name}</option>))
                                        return langOptions
                                    })()}
                                </select>
                                <br/>

                                <select name={"gender"} onChange={(e) => {
                                    if (e.target.value != "def") genderStatusSet(true)
                                    else genderStatusSet("def")
                                }}>
                                    <option value={"def"}>Select your gender</option>
                                    <option value={"male"}>Male</option>
                                    <option value={"female"}>Female</option>
                                </select>

                                <label>
                                    <input type={"checkbox"} onChange={(e) => agreement2StatusSet(!!e.target.checked)}/>
                                    <p>Agreement 2</p>
                                </label>

                                <div id={"fatal"}>{fatal}</div>
                                <button
                                    disabled={!(countryStatus != "def" && genderStatus != "def" && dateStatus && agreement2Status)}>Move me!
                                </button>
                            </form>
                        </>
                        )
                    : data.push(`Our page requires you to login. If you don't have an account then please register. It's free`)
                return data
            })()}</>
        )}
    </>
}
Home.getInitialProps = async (props) => {
    return {lang: props.query}
}
export default Home