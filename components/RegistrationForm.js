"use strict"

import React, {Component} from 'react'
import axios from 'axios'

import Validators, {inputLengthRange} from "./collectors/Validators"
import Database from "./collectors/DatabaseCollector"
import Lang from "./collectors/LangCollector"

export default class RegistrationForm extends Component {
    connection = null

    state = {
        disableSubmit: true,
        disableInputs: false,

        exceptions: {
            username: this.props.predefined.username,
            email: this.props.predefined.email,
            password: this.props.predefined.password,
            checkbox1: true
        },
        exceptionsToShow: {}
    }

    async submitForm(e){
        e.preventDefault()
        var target = e.target
        if(!this.state.disableSubmit && !await this.checkIfExceptions()){
            let formData = new FormData(target)

            this.setState({disableSubmit: true, disableInputs: true})
            return new Promise((resolve, reject) => {
                grecaptcha.ready(() => {
                    grecaptcha.execute(this.props.serverData.siteKey).then(async (token) => {
                        formData.append("token", token)

                        await axios({
                            method: "POST",
                            url: "/registration",
                            data: formData
                        }).then(res => res.data)
                            .then(async (response) => {
                                if(response.err) {
                                    if(response.validations) {
                                        for(let field in response.validations){
                                            if(response.validations[field]) {
                                                if(field != "token") {
                                                    let errorContent = await Lang.getFunctionPack(response.validations[field], [this.props.langPack[field].toLowerCase(), inputLengthRange[field].min, inputLengthRange[field].max], this.props.query.lang)

                                                    this.setObjectInState("exceptions", {[field]: errorContent})
                                                    this.setObjectInState("exceptionsToShow", {[field]: errorContent})
                                                }
                                                else this.setObjectInState("exceptionsToShow", {fatal: this.props.langPack[response.validations[field]]})
                                            }
                                        }
                                    }
                                    else this.setObjectInState("exceptionsToShow", {fatal: "Sample message"})

                                    reject()
                                }
                                else resolve()
                            })
                            .catch((err) => {
                                this.setObjectInState("exceptionsToShow", {fatal: this.props.langPack.E1002})
                                reject(err)
                            })
                    })
                })
            }).then(
                () => {
                    return console.log("noError")
                },
                (err) => {
                    //Need to log err
                    for(let input of target) {
                        if(!input.type) return(false)
                        if(input.type == "password") input.value = ""
                        if(input.type == "checkbox") input.checked = false

                        this.setState({disableInputs: false})
                    }
                }
            )
        }
    }

    setObjectInState(inStateObjectName, toSave){
        let inStateObject = this.state[inStateObjectName]
        Object.assign(inStateObject, toSave)
        this.setState({[inStateObjectName]: inStateObject})
    }

    async checkIfExceptions() {
        var exceptions = [], good = true
        for(let exc in this.state.exceptions) exceptions.push(this.state.exceptions[exc])
        await Promise.all(exceptions).then(resps => {
            for(let resp of resps) if(resp) {
                good = false
            }
        })
        return !good
    }

    render(){
        return(
            <>
                <script src={`https://www.google.com/recaptcha/api.js?render=${this.props.serverData.siteKey}`}></script>
                <form onSubmit={(e) => this.submitForm(e)}>
                    <label>
                        <input name={"username"} disabled={this.state.disableInputs}
                               onChange={async (e) => {
                                   this.setState({disableSubmit: true})
                                   await this.setObjectInState("exceptions", {[e.target.name]: Validators[e.target.name](this.props.query.lang, this.props.langPack[e.target.name], e.target.value)})
                               }}
                               onBlur={async (e) => {
                                   e.persist()
                                   Promise.all([this.state.exceptions[e.target.name]]).then(exc => {
                                       if(!exc[0])
                                           new Promise((resolve) => {
                                               Database.findOne("main", "users", {["username"]: e.target.value.trim().toLowerCase()}).then(
                                                   async (resp) => {
                                                       if (resp) {
                                                           await this.setObjectInState("exceptions", {username: Lang.getFunctionPack("ALREADYEXISTS", [this.props.langPack["username"].toLowerCase()], this.props.query.lang)})
                                                           this.setObjectInState("exceptionsToShow", {[e.target.name]: await Lang.getFunctionPack("ALREADYEXISTS", [this.props.langPack["username"].toLowerCase()], this.props.query.lang)})
                                                       }
                                                        else if(!await this.checkIfExceptions()) this.setState({disableSubmit: false})
                                                       resolve()
                                                   },
                                                   () => {
                                                       this.setObjectInState("exceptionsToShow", {fatal: "Unexpected error!"})
                                                       resolve()
                                                   }
                                               )
                                           })
                                       else this.setObjectInState("exceptionsToShow", {[e.target.name]: exc[0]})
                                   })
                               }}
                               onFocus={(e) => {
                                   if (this.state.exceptionsToShow[e.target.name] != "") this.setObjectInState("exceptionsToShow", {[e.target.name]: ""})
                               }}
                               placeholder={this.props.langPack.username}/>
                        <div id={"emailError"}>{this.state.exceptionsToShow.username}</div>
                    </label>
                    <label>
                        <input name={"email"} disabled={this.state.disableInputs}
                               onChange={async (e) => {
                                   this.setState({disableSubmit: true})
                                   await this.setObjectInState("exceptions", {[e.target.name]: Validators[e.target.name](this.props.query.lang, this.props.langPack[e.target.name], e.target.value)})
                               }}
                               onBlur={async (e) => {
                                   e.persist()
                                   Promise.all([this.state.exceptions[e.target.name]]).then(exc => {
                                       if(!exc[0])
                                           new Promise((resolve) => {
                                               Database.findOne("main", "users", {["email"]: e.target.value.trim().toLowerCase()}).then(
                                                   async (resp) => {
                                                       if (resp) {
                                                           await this.setObjectInState("exceptions", {email: Lang.getFunctionPack("ALREADYEXISTS", [this.props.langPack["email"].toLowerCase()], this.props.query.lang)})
                                                           this.setObjectInState("exceptionsToShow", {[e.target.name]: await Lang.getFunctionPack("ALREADYEXISTS", [this.props.langPack["email"].toLowerCase()], this.props.query.lang)})
                                                       }
                                                       else if(!await this.checkIfExceptions()) this.setState({disableSubmit: false})
                                                         resolve()
                                                   },
                                                   () => {
                                                       this.setObjectInState("exceptionsToShow", {fatal: "Unexpected error!"})
                                                       resolve()
                                                   }
                                               )
                                           })
                                       else this.setObjectInState("exceptionsToShow", {[e.target.name]: exc[0]})
                                   })
                               }}
                               onFocus={(e) => {
                                   if (this.state.exceptionsToShow[e.target.name] != "") this.setObjectInState("exceptionsToShow", {[e.target.name]: ""})
                               }}
                               placeholder={this.props.langPack.email}/>
                        <div id={"emailError"}>{this.state.exceptionsToShow.email}</div>
                    </label>
                    <label>
                        <input name={"password"} type={"password"} disabled={this.state.disableInputs}
                               onChange={async (e) => {
                                   await this.setObjectInState("exceptions", {[e.target.name]: Validators[e.target.name](this.props.query.lang, this.props.langPack[e.target.name], e.target.value)})
                                   await this.checkIfExceptions() ? this.setState({disableSubmit: true}) : this.setState({disableSubmit: false})
                               }}
                               onBlur={(e) => {
                                   e.persist()
                                   if (this.state.exceptions[e.target.name]) Promise.all([this.state.exceptions[e.target.name]]).then(exc => this.setObjectInState("exceptionsToShow", {[e.target.name]: exc[0]}))
                               }}
                               onFocus={(e) => {
                                   if (this.state.exceptionsToShow[e.target.name] != "") this.setObjectInState("exceptionsToShow", {[e.target.name]: ""})
                               }}
                               placeholder={this.props.langPack.password}/>
                        <div id={"passwordError"}>{this.state.exceptionsToShow.password}</div>
                    </label>

                    <label style={{display: "flex"}}><input type={"checkbox"} name={"checkbox1"} disabled={this.state.disableInputs}
                    onChange={async (e) => {
                        await this.setObjectInState("exceptions", {[e.target.name]: new Promise((r) =>  r(!e.target.checked))})
                        await this.checkIfExceptions() ? this.setState({disableSubmit: true}) : this.setState({disableSubmit: false})
                    }}
                    ></input><p>Agreement number 1</p></label>

                    <label>
                        <div id={"fatalError"}>{this.state.exceptionsToShow.fatal}</div>
                        <button disabled={this.state.disableSubmit}> {this.props.langPack.LoginConfirm}</button>
                    </label>
                </form>
            </>
        )
    }
}