import axios from "axios/index";

export default async (varName, type, message) => {
    //There were strange fs errors in node while trying to use node4js typescript version, so I decided just to log everything with node

    let serverPath = ""
    if(process.env.npm_package_proxy) serverPath = process.env.npm_package_proxy

    await axios({
        method: 'post',
        url: serverPath + '/logMe',
        data: {varName, type, message}
    }).catch((res) => {
        //A way to handle error in saving logs
    })
}