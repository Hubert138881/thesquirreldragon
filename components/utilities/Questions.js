import React from "react"
import axios from "axios"

async function questionMgr(option, data) {
    let serverPath = ""
    if(process.env.npm_package_proxy) serverPath = process.env.npm_package_proxy

    if( option != "readMostAccurate" ||
        option != "proposeQuestion" ||
        option != "answerQuestion" ||
        option != "flagQuestion") return ({err: "BADOPTION"})

    await axios({
        method: 'post',
        url: serverPath + '/questionsProcess',
        data: {
            option,
            data
        }
    }).catch((res) => {
        return ({err: "CONNECTIONERROR"})
    })
}
export default questionMgr;