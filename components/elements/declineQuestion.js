import React from "react"
import QuestionCollector from "../collectors/QuestionCollector"

export default class DeclineQuestion extends React.Component{
    state = {}
    render(){
        return(<>
            <button onClick={e => {
                e.preventDefault()
                this.setState(prev => ({active: !prev.active}))
            }}>Decline</button>
            {this.state.active ? <form onSubmit={e => {
                e.preventDefault()
                let newForm = new FormData(e.target)
                newForm.append("id", this.props.id)
                newForm.append("option", "declineQuestion")
                QuestionCollector(newForm).then(
                    () => {
                        alert("Question has benn declined")
                        this.props.success ? this.props.success() : void(0)
                    },
                    err => {
                        this.props.error ? this.props.error(err) : void(0)
                    }
                )
            }}>
                <input type="text" name="reason" placeholder="Reason"/>
                <button>Submit</button>
            </form> : <></>}
        </>)
    }
}