import React from "react"
import questionCollector from '../collectors/QuestionCollector'

export default class GetQuestions extends React.Component {
    state = {
        qst: false
    }
    getQuestions() {
        questionCollector({
            option: "getMostAccurate",
        }).then(
            data => {
                console.log(data)
                this.setState({qst: data})
            },
            err => console.log(err)
        )
    }
    componentDidMount() {
        if(this.props.noButton) this.getQuestions() 
    }

    render() { 
        return(<>
            {this.props.noButton ?  <></> : 
                <form onSubmit={e => {
                    e.preventDefault()
                    this.getQuestions()
            }}><button>Get question</button></form>}
            {this.state.qst ? <form onSubmit={e => {
                e.preventDefault()
                let newForm = new FormData(e.target)
                newForm.append("id", this.state.qst["_id"])
                newForm.append("type", this.state.qst.type)
                newForm.append("option", "answerQuestion")

                questionCollector(newForm).then(
                    data => {
                        Location.reload()
                    },
                    err => console.log(err)
                )
            }}>
                <p>{this.state.qst.translations.en.Q}</p>
                <label><input type="radio" name={"answer"} onClick={() => this.setState({chosenAnswer: true})} value={"A"}/><p>{this.state.qst.type == '0' ? "yes" : this.state.qst.translations.en.A}</p></label>

                <label><input type="radio" name={"answer"} onClick={() => this.setState({chosenAnswer: true})}  value={"B"}/><p>{this.state.qst.type == '0' ? "no" : this.state.qst.translations.en.B}</p></label>

                {this.state.qst.type == '1' ? <>
                    <label><input type="radio" name={"answer"} onClick={() => this.setState({chosenAnswer: true})}  value={"C"}/><p>{this.state.qst.translations.en.C}</p></label>

                    <label><input type="radio" name={"answer"} onClick={() => this.setState({chosenAnswer: true})}  value={"D"}/><p>{this.state.qst.translations.en.D}</p></label>
                </> : <></>}
                
                <button disabled={!this.state.chosenAnswer}>Submit</button>
                <button onClick={e => {
                    e.preventDefault()
                    this.setState({flag: true})
                }}>Flag</button>
            </form> : void(0)}
            {this.state.flag ? <form onSubmit={e => {
                e.preventDefault()
                let newForm = new FormData(e.target)
                newForm.append("id", this.state.qst["_id"])
                newForm.append("option", "flagQuestion")
                newForm.append("lang", this.props.query.lang)
                questionCollector(newForm).then(
                    data => {
                        alert("Your question has been added")
                    },
                    err => console.log(err)
                )
            }}>
                <input type="text" name={"reason"} placeholder={"Reason"}/>
                <button>Submit</button>
            </form> : void(0)}
        </>)
    }
}