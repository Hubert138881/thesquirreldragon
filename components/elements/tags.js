import React from "react"
import questionCollector from "../collectors/QuestionCollector";

const treeRows = ["1", "2", "3"]

export default class Tags extends React.Component{

    state = {
        level: "1"
    }

    constructor() {
        super()
        questionCollector({
            option: "treeOperations",
            treeOperation: "getTags"
        }).then(
            data => {
                this.tags = data.list
            },
            err => console.log(err)
        )
    }

    render() {
        return (<>
            <select name="rowsNum" onChange={e => {
                /*FIRST LEVEL - TAGS*/
                this.setState({
                    tag2: false,
                    tag3: false
                })
                for (let element of document.getElementsByClassName("clearTag1"))
                    element.checked = false

                if (this.tags != {}) this.setState(
                    {level: e.target.value})
            }}>
                {(() => {
                    let optArr = []
                    treeRows.map(rowNum => optArr.push(<option value={rowNum}>Row {rowNum}</option>))
                    return optArr
                })()}
            </select>
            {<div>{this.state.level == "1" ? <>
                    <input type="text" name={"tag1"} placeholder={"create a new tag for 1st level question"}/>
                    <form onSubmit={e => {
                        e.preventDefault()
                    }}>
                        <button>Check</button>
                    </form>
                </>
                : <>
                    <p>Choose the first level tag you want to pin a question to</p>
                    {(() => {
                        let tagsArr = []
                        for (let tagName in this.tags) {
                            tagsArr.push(<label><input type="radio" name={"tag1"} className={"clearTag1"}
                                                       value={tagName} onChange={e => {
                                e.persist()
                                this.setState({tag2: true})
                                this.setState(prevVal => ({
                                    tag1_v: {
                                        ...prevVal.tag1_v,
                                        [tagName]: e.target.checked
                                    }
                                }))
                            }}/><p>{tagName}</p>
                            </label>)
                        }
                        return tagsArr
                    })()}
                </>}</div>
                /*END FIRST LEVEL - TAGS*/
            }
            {this.state.tag2 ? <div>
                {(() => {
                    /*SECOND LEVEL - TAGS*/
                    if (this.state.level == "2") return (<>
                        <input type="text" name={"tag2"} placeholder={"Write new second level tag"}/>
                        <form onSubmit={e => {
                            e.preventDefault()
                        }}>
                            <button>Check</button>
                        </form>
                    </>)
                    else {
                        let tagsArr = [<p>Choose the second level tag you want to pin a question to</p>]
                        for (let firstLvlTag in this.state.tag1_v)
                            if (this.state.tag1_v[firstLvlTag])
                                for (let secondLvlTag in this.tags[firstLvlTag])
                                    tagsArr.push(<label>
                                        <input type={"radio"} name={"tag2"} value={secondLvlTag} onChange={e => {
                                            e.persist()
                                            this.setState({tag3: true})
                                        }}/><p>{secondLvlTag}</p></label>)
                        return tagsArr
                    }
                    /*END SECOND LEVEL - TAGS*/
                })()}
            </div> : void (0)}
            {/*THIRD LEVEL - TAGS*/}
            {this.state.tag3 && this.state.level == '3' ? <div>
                <input type="text" name={"tag3"} placeholder={"Write new third level tag"}/>
                <form onSubmit={e => {
                    e.preventDefault()
                }}>
                    <button>Check</button>
                </form>
            </div> : void (0)}
        </>)
    }
}