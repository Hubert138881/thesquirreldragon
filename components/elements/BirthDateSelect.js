import React from 'react'
import months from "../utilities/months.json"
export default class BirthDate extends React.Component {
    state = {
        good: false,

        dayVal: undefined,
        monthVal: undefined,
        yearVal: undefined,
    }

    checkIfGood() {
        let day = this.state.dayVal,
        mth = this.state.monthVal,
        yr = this.state.yearVal
        return new Promise((resolve, reject) => {
            if(!day ||
                mth == "def" ||
                mth < 0 ||
                mth > 12 ||
                !yr ||
                isNaN(day) ||
                day <= 0 ||
                isNaN(yr)) reject()
            else {
                try {
                    let currYr = new Date().getFullYear()
                    if(currYr - 130 >= yr || currYr - 5 <= yr) return(reject())

                    let days
                    if(mth == 1 || mth == 3 || mth == 5 || mth == 7 || mth == 8 || mth == 10 || mth == 12) days = 31
                    else days = 30
                    if(mth == 2)
                        if(yr % 4) days = 28
                        else days = 29
                    if(day > days) return(reject())
                    resolve()
                }
                catch(err){
                   reject()
                }
            }
        }).then(
            () =>  this.props.changed(true),
            () => this.props.changed(false)
        )
    }

    render(){
        return(
            <>
                <input name={"day"} placeholder={"day"} onChange={async (e) => {
                    await this.setState({dayVal: e.target.value})
                    this.checkIfGood()
                }}/>
                <select name={"month"} onChange={async (e) => {
                    await this.setState({monthVal: e.target.value})
                    this.checkIfGood()
                }}>
                    <option value={"def"}>month</option>
                    {(() => {
                        let monthsOpts = []
                        for(let month of months) monthsOpts.push(<option value={month.value}>{this.props.langPack[month.name]}</option>)
                        return monthsOpts
                    })()}
                </select>
                <input name={"year"} placeholder={"year"} onChange={async (e) => {
                    await this.setState({yearVal: e.target.value})
                    this.checkIfGood()
                }}/>
            </>
        )
    }
}