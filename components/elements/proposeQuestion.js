import React, {Component} from "react"
import update from 'react-addons-update'
import languages from '../utilities/languages.json'
import SelectCategories from './selectCategories'
import questionCollector from '../collectors/QuestionCollector'

export class NewQuestion extends Component {
    state = {
        qstTypeCheck: 0,
        translations: [],
        tranBlckNo: 0,
        howManyBlck: 0
    }

    constructor() {
        super()
        var langNumb = 0;
        for(let language of languages) if(language.code != "en") langNumb++
        this.langNumb = langNumb
    }

    getState() {
        return this.state
    }

    render() {
        return(<>
            <label><input type={"radio"} name={"qstType"} checked={this.state.qstTypeCheck == 1 ? true : false} onChange={() => {this.setState({qstTypeCheck: 1})}}/><p>Question with 4 possible answers</p></label>
            <label><input type={"radio"} name={"qstType"} checked={this.state.qstTypeCheck == 2 ? true : false} onChange={() => {this.setState({qstTypeCheck: 2})}}/><p>Question with 2 possible answers</p></label>

            <p>Please, fill up the inputs below in english</p>
            <input type="text" name={"Q"} placeholder={"Your question"}/> <br/>
            {(() => {
                switch(this.state.qstTypeCheck) {
                    case 1:
                        return(<>
                            <input type="text" name={"A"} placeholder={"Answer A"}/> <br/>
                            <div>{this.state["A"]}</div>
                            <input type="text" name={"B"} placeholder={"Answer B"}/> <br/>
                            <div>{this.state["B"]}</div>
                            <input type="text" name={"C"} placeholder={"Answer C"}/> <br/>
                            <div>{this.state["C"]}</div>
                            <input type="text" name={"D"} placeholder={"Answer D"}/> <br/>
                            <div>{this.state["D"]}</div>
                        </>)
                    case 2:
                        return(<>
                            <input type="text" name={"A"} placeholder={"Answer A"}/> <br/>
                            <div>{this.state["A"]}</div>
                            <input type="text" name={"B"} placeholder={"Answer B"}/> <br/>
                        </>)
                }
            })()}
        </>)
    }
}

export default class ProposeQuestion extends Component {
    constructor(){
        super()
        this.newQuestion = React.createRef()
    }
    submitForm(e){
        let newForm = new FormData(e.target)
        newForm.append("option", "proposeQuestion")
        newForm.append("type", this.newQuestion.current.getState().qstTypeCheck)
        questionCollector(newForm).then(
            () => {
                if(this.props.success) this.props.success()
            },
            (err) => {
                if(this.props.error) this.props.error(err)
            }
        )
    }

    render() {
        return(
            <>
                <form action={""} onSubmit={(e) => {
                    e.preventDefault()
                    this.submitForm(e)
                }}>
                    <SelectCategories {...this.props}/>
                    <NewQuestion ref={this.newQuestion}/>
                    <button>Submit</button>
                </form>
            </>
        )
    }
}