import React from "react"
import {QuestionReader, deleteQuestion} from "./acceptQuestions"
import languages from "../utilities/languages.json"
import questionCollector from '../collectors/QuestionCollector'

export default class AddTranslations extends React.Component {
    state = {
        questions: [],
        amount: 1
    }
    render(){
        return(<>
            {this.state.questions.map(qst => {
                return(<>
                    <p>English</p>
                    <p>Timer: {this.state["time_"+qst["_id"]]}</p>
                    <p>Q: {qst.translations.en.Q}</p>
                    {qst.type != 0 ? <>
                        <p>A: {qst.translations.en.A}</p>
                        <p>B: {qst.translations.en.B}</p>
                        {qst.type == 1 ? <>
                            <p>C: {qst.translations.en.C}</p>
                            <p>D: {qst.translations.en.D}</p>
                        </> : <></>}
                    </> : <></>}
                    {Object.keys(this.state.additional).map(lang => {
                        if(!qst.translations[lang]) return(<form onSubmit={e => {
                            e.preventDefault()
                            let newForm = new FormData(e.target)
                            newForm.append("lang", lang)
                            newForm.append("type", qst.type)
                            newForm.append("id", qst["_id"])
                            newForm.append("option", "acceptTranslation")
 
                            questionCollector(newForm).then(
                                () => alert("Translation have been accepted"),
                                err => console.log(err)
                            )
                        }}>
                            <p>{lang}</p>
                            <input type="text" name="Q" placeholder="Q"/>
                            {qst.type != 0 ? <>
                                <input type="text" name="A" placeholder="A"/>
                                <input type="text" name="B" placeholder="B"/>
                                {qst.type == 1 ? <>
                                <input type="text" name="C" placeholder="C"/>
                                <input type="text" name="D" placeholder="D"/>
                                </> : <></>}
                            </> : <></>}
                            <button>Submit</button>
                        </form>)
                    })}
                </>)
            })}
            <form action="" onSubmit={e => {
                e.preventDefault()
            }}>
                {languages.map(lang => {
                    if(lang.code != "en") return(<label><input type="checkbox" onChange={e => {
                        this.setState(prev => ({
                            additional: {
                                ...prev.additional,
                                [lang.code]: !prev[lang.code]
                            }
                        }))
                    }}/><p>{lang.name}</p></label>)
                })}
                <QuestionReader {...this.props} parent={this} option={"getNotTranslated"}/>
            </form>
        </>)
    }
}