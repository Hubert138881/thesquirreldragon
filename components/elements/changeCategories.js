import React from "react"
import DatabaseCollector from '../collectors/DatabaseCollector'
import QuestionsCollector from '../collectors/QuestionCollector'

export default class ChangeCategories extends React.Component {
    state = {
        categories: []
    }

    constructor() {
        super() 
        DatabaseCollector.findOne("main", "questions", {id: "categories"}).then(
            resp => {
                let cathLabel = []
                Object.keys(resp.categories).map(cath => 
                    cathLabel.push(<label><input type="checkbox" name={cath}/>{resp.categories[cath].name}</label>))
                this.setState(prev => ({categories: [...prev.categories, ...cathLabel]}))
            },
            err => console.log(err)
        )
    }

    render(){
        return(<>
            <p>Choose the cathegories you want to get your questions about</p>
            <form onSubmit={e => {
                e.preventDefault()
                let newForm = new FormData(e.target)
                newForm.append("option", "changeCategories")
                QuestionsCollector(newForm).then(
                    () => {
                        this.props.changed()
                    }, 
                    err => console.log(err)
                )
            }}>
                {this.state.categories}
                <button>Submit</button>
            </form>
        </>)
    }
}