import React from "react"
import Timer from "../utilities/Timer"
import questionCollector from '../collectors/QuestionCollector'
import {NewQuestion} from "./proposeQuestion"
import Tags from "./tags"
import SelectCategories from "./selectCategories"

export default class AddQuestion extends React.Component{
    constructor(){
        super()
        this.newQestion = React.createRef()
    }
    render() {
        return(<>
            <form onSubmit={e => {
                e.preventDefault()
                let newForm = new FormData(e.target)
                newForm.append("type", this.newQestion.current.getState().qstTypeCheck)
                newForm.append("option", "addQuestion")
                questionCollector(newForm).then(
                    data => {
                        alert("Your question has been added")
                    },
                    err => console.log(err)
                )
            }}>
                <SelectCategories both={true}/>
                <NewQuestion ref={this.newQestion}/>
                <button>Add new question</button>
            </form>
        </>)
    }
}