import React from "react"
import QuestionsCollector from '../collectors/QuestionCollector'
import SelectCategories from '../elements/selectCategories'

export default class AddSubcategory extends React.Component {
    render() {
        return(<form onSubmit={e => {
            e.preventDefault()
            let newForm = new FormData(e.target)
            newForm.append("option", "addSubcategory")
            QuestionsCollector(newForm).then(
                    () => {
                        if(this.props.success) this.props.success()
                    },
                    err => {
                        if(this.props.error) this.props.error(err)
                    }
                )
        }}> 
            <input type="text" name="Q" placeholder="Question"/>
            <input type="text" name="subF" placeholder="Subcategory"/>
            <input type="text" name="sub" placeholder="Short form"/>
            <SelectCategories {...this.props} both={true}/>
            <button>Add subcategory</button>
        </form>)
    }
}