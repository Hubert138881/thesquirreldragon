import React from "react"
import DatabaseCollector from "../collectors/DatabaseCollector"
import questionCollector from '../collectors/QuestionCollector'
import Timer from "../utilities/Timer"
import SelectCategories from "./selectCategories"
import DeclineQuestion from "./declineQuestion"
import {deleteQuestion, acceptIn, setTimerInterval} from "./acceptQuestions"

export default class ModifyQuestions extends React.Component {
    state = {
        id: "7130346bcf0834376cba56",
        qst: []
    }
    getQuestion() {
        let id = this.props.id ? this.props.id : this.state.id
        questionCollector({option: "extendTime", id}).then(
            () => {
                return DatabaseCollector.findOne("main", "questions", {_id: id})
            }
        ).then(res => {
            if(!res){throw "NO_QUESTION"}
            let lastMod = new Date(res.lastModerationDate)
            //if((new Date()).getTime() - lastMod.getTime() < 1000 * 3600 * 7) {throw "NO_TIME"}
            this.setState(prev => ({
                ...prev,
                qst: [...prev.qst, res],
                [`timer_${res["_id"]}`]: new Timer(5, 0),
                ['trans_'+res["_id"]]: [],
                ...(() => {
                    let langData = {}
                    Object.keys(res.translations).map(lang =>
                        Object.keys(res.translations[lang]).map(sign =>
                            langData[`${lang}_${sign}_${res["_id"]}`] = res.translations[lang][sign]
                        )
                    )
                    return langData
                })(),
                //set timers for new questions (only when id is not specified)
                [`timer_${res["_id"]}`]: new Timer(acceptIn.min, acceptIn.sec)
            }))
            if(!this.props.id) setTimerInterval([res], this)
        })
        .catch(err => {
            console.log(err)
        })
    }

    componentDidMount() {
        if(this.props.id) this.getQuestion()
    }

    render() {
        return(<>
            {!this.props.id ? <form onSubmit={e => {
                e.preventDefault()
                this.getQuestion()
            }}>
                <input type="text" placeholder={"enter question Id"} onChange={e => this.setState({id: e.target.value})}/>
                <button>Get a question</button>
            </form> : <></>}


            {this.state.qst.map(qst => {
                let s = `_${qst["_id"]}`, transArr = []
 
                Object.keys(qst.translations).map(lang => {
                    transArr.push(<form onSubmit={e => this.submitChanges(e, qst["_id"], "trans", lang)}>
                        <p>{lang}</p>

                        <p>Q: <input type="text" name="Q" value={this.state[`${lang}_Q${s}`]} onChange={e => this.setState({[`${lang}_Q${s}`]: e.target.value})}/></p>

                        {qst.type != "0" ? <>
                            <p>A: <input type="text" name="A" value={this.state[`${lang}_A${s}`]} onChange={e => this.setState({[`${lang}_A${s}`]: e.target.value})}/></p>
                            
                            <p>B: <input type="text" name="B" value={this.state[`${lang}_B${s}`]} onChange={e => this.setState({[`${lang}_B${s}`]: e.target.value})}/></p>
                            
                            {qst.type == '1' ? <>
                                <p>C: <input type="text" name="C" value={this.state[`${lang}_C${s}`]} onChange={e => this.setState({[`${lang}_C${s}`]: e.target.value})}/></p>
                                
                                <p>D: <input type="text" name="D" value={this.state[`${lang}_D${s}`]} onChange={e => this.setState({[`${lang}_D${s}`]: e.target.value})}/></p>
                            </> : <></>}
                        </> : <></>}
                        <button>Submit</button>
                    </form>)
                })
                return(<>
                    <p>Status: {qst.deleted ? "Declined" : "Active"}</p>
                    <p>Timer: {this.state['time_'+qst["_id"]]}</p>
                    <p>ID: {qst["_id"]}</p>
                    <p>Question: {qst.translations.en.Q}</p>

                    {<label><p>Subcategory</p><button onClick={e => {
                        e.preventDefault()
                        this.setState(prev => ({['subShow'+s]: !prev['subShow'+s]}))
                    }}>v</button></label>}
                    {this.state['subShow'+s] ? <form onSubmit={e => this.submitChanges(e, qst["_id"], "sub")}>
                        <SelectCategories {...this.props} 
                            both={true}
                            selected={qst.belongsTo}
                        />
                        <button>Submit</button>
                    </form> : <></>}

                    {<label><p>Translations</p><button onClick={e => {
                        e.preventDefault()
                        this.setState(prev => ({['transShow'+s]: !prev['transShow'+s]}))
                    }}>v</button></label>}
                    {this.state['transShow'+s] ? transArr : <></>}

                    {qst.type != 0 && !qst.deleted ? 
                        <DeclineQuestion 
                            id={qst["_id"]}
                            success={() => {
                                deleteQuestion(qst["_id"], this)
                            }}
                            error={err => console.log(err)}
                        /> : 
                    <></>}
                </>)
            })}
        </>)
    }
    submitChanges(e, id, type, lang){
        e.preventDefault()
        let newForm = new FormData(e.target)
        newForm.append("id", id)
        newForm.append("type", type)
        if(lang) newForm.append("lang", lang)
        newForm.append("option", "modifyQuestion")
        
        questionCollector(newForm).then(
            () => alert("Data changes has been saved"),
            err => console.log(err)
        )
    } 
}