import axios from "axios/index";

export default class UpdateCollector {
    static rulesAccept(form) {
        return new Promise((resolve, reject) => {
            axios({
                method: 'post',
                data: form,
                url: `/updates-rulesAccept`,
            }).then(res => res.data).then(
                res => {
                    if(res.err) reject(res.validations)
                    else resolve()
                }
            ).catch((err) => {
                reject()
                //save err 1003 to logs
            })
        })
    }
}