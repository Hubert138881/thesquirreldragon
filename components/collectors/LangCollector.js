import axios from "axios/index";
import CookieOperations from "./CookieOperationsCollector";

/**
* @usage: retrieving and setting language packs
*
* > getLangPack
* @work: tries to make an ajax request to the route /langPack and gives response to full object
* @success: resolve language pack with data
* @fail: set error rejections
* > setLang
* @work: sets cookies with information about preferred language pack then reloads the page desiring node  to proceed with the cookie
 */

export default class LangCollector {
    static getFunctionPack(name, parameters, lang) {
        let serverPath = ""
        if(process.env.npm_package_proxy) serverPath = process.env.npm_package_proxy

        return new Promise((resolve, reject) => {
            axios({
                method: 'post',
                url: serverPath + "/functionLangPack",
                data: {
                    name, parameters, lang
                }
            }).then(res => res.data).then(
                res => {
                    if(res.error) reject()
                    else resolve(res.data)
                }
            )
        })
    }

    static getLangPack(lang, headers, cookies){
        return new Promise((resolve, reject) => {
            axios({
                method: 'post',
                url: process.env.npm_package_proxy + '/langPack',
                data: {
                    lang, headers, cookies
                }
            }).then(res => res.data).then(
                res => {
                    if(res.error) reject({error: res.error})
                    else resolve(res)
                }
            ).catch(() => reject({error: "E1000"}))
        })
    }
    static forceGetLangPack(lang, headers, cookies) {
        return new Promise((resolve, reject) => {
            axios({
                method: 'post',
                url: process.env.npm_package_proxy + '/forceLangPack',
                data: {
                    lang, headers, cookies
                }
            }).then(res => res.data).then(
                res => {
                    if(res.err) reject({error: res.error})
                    else resolve(res.data)
                }
            ).catch(() => reject({error: "E1000"}))
        })
    }

    static setLang(lang, duration){
        if(CookieOperations.get().lang != lang) {
            CookieOperations.set("lang", lang, duration)
            location.reload()
        }
    }
}