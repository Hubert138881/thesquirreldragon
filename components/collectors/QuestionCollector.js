import react from "react"
import axios from "axios"

/*
* option:
*   readMostAccurate
*   answerQuestion
*   proposeQuestion
*   flagQuestion
*   addQuestion
*   modifyQuestion
*   banQuestion
*   acceptQuestion
*   modifyFlagged
 */


function makeReq(form) {

    let serverPath = ""
    if(process.env.npm_package_proxy) serverPath = process.env.npm_package_proxy

    return new Promise((resolve, reject) => {
        axios({
            method: 'post',
            url: `${serverPath}/questionsProcess`,
            data: form
        }).then(res => res.data).then(
            res => {
                if(res.err) reject(res)
                else resolve(res.data)
            }
        ).catch((err) => {
            reject({msg: "FATAL"})
        })
    })
}
export default makeReq