/**
 * @usage: simplify operations on cookie files
 *
 * > get
 * @work gets all of the cookies and packs them into an object (get().cookieName)
 * > set
 * @work set a cookie in document.cookie
 * > delete
 * @work delete a cookie (set for chosen-name cookie an expiration date of 0)
*/

export default class CookieOperationsCollector {
    static get(){
        let pairs = document.cookie.split(";");
        let cookies = {};
        for (var i=0; i<pairs.length; i++){
            let pair = pairs[i].split("=");
            cookies[(pair[0]+'').trim()] = unescape(pair.slice(1).join('='));
        }
        return cookies;
    }
    static set(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }
    static delete(name) {
        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
}