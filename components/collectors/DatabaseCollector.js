import axios from "axios"

export default class Database {
    static async connect(db, collection, operation, queryData, options){
        let serverPath = ""
        if(process.env.npm_package_proxy) serverPath = process.env.npm_package_proxy

        return await new Promise((resolve, reject) => {
            axios({
                method: 'post',
                url: `${serverPath}/clientDb`,
                withCredentials: true,
                data: {
                    db, collection, operation, queryData, options,
                }
            }).then(res => res.data).then(
                res => {
                    if(res.err) reject()
                    else resolve(res.response)
                }
            ).catch((err) => {
                reject()
                //save err 1003 to logs
            })
        })
    }

    static findOne(db, collection, queryData){
        return this.connect(db, collection, "findOne", queryData)
    }

    static insertOne(db, collection, queryData){
        return this.connect(db, collection, "insertMany", queryData)
    }

    /*static updateOne(db, collection, queryData, toUpdate){
        return this.connect(db, collection, "updateOne", queryData, toUpdate)
    }*/

    static deleteOne(db, collection, queryData){
        return this.connect(db, collection, "deleteOne", queryData)
    }

    static find(db, collection, queryData, options){
        return this.connect(db, collection, "find", queryData, options)
    }

    static insert(db, collection, queryData){
        return this.connect(db, collection, "insertMany", queryData)
    }

    /*static update(db, collection, queryData, toUpdate){
        return this.connect(db, collection, "updateMany", queryData, toUpdate)
    }*/

    static delete(db, collection, queryData){
        return this.connect(db, collection, "deleteMany", queryData)
    }
}