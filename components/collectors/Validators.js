import Lang from './LangCollector'

export const inputLengthRange = {
    username: {
        min: 4,
        max: 15
    },
    email: {
        min: 5,
        max: 0
    },
    password: {
        min: 8,
        max: 64
    },
    confirmation: {
        min: 0,
        max: 0
    },
    profilePicture: {
        min: 0,
        max: 0
    }
}

export default class Validators {
    static async email(lang, name, content) {
        content = content.trim()
        name = name.toLowerCase()

        let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        if (content == "") return await Lang.getFunctionPack("NOTHING", [name], lang)
        if (content.length < inputLengthRange.email.min) return await Lang.getFunctionPack("TOOSHORT", [name, inputLengthRange.email.min], lang)
        if (!content.match(regex)) return await Lang.getFunctionPack("WRONGSYNTAX", [name], lang)
        return false
    }

    static async  username(lang, name, content) {
        content = content.trim()
        name = name.toLowerCase()

        let regex = /^([a-zA-Z0-9])+$/

        if (content == "") return await Lang.getFunctionPack("NOTHING", [name], lang)
        if (content.length < inputLengthRange.username.min) return await Lang.getFunctionPack("TOOSHORT", [name, inputLengthRange.username.min], lang)
        if (content.length > inputLengthRange.username.max) return await Lang.getFunctionPack("TOOLONG", [name, inputLengthRange.username.min, inputLengthRange.username.max], lang)
        if (!content.match(regex)) return await Lang.getFunctionPack("WRONGSYNTAX", [name], lang)
        return false
    }

    static async password(lang, name, content) {
        content = content.trim()
        name = name.toLowerCase()

        if (content == "") return await Lang.getFunctionPack("NOTHING", [name], lang)
        if (content.length < inputLengthRange.password.min) return await Lang.getFunctionPack("TOOSHORT", [name, inputLengthRange.password.min], lang)
        if (content.length > inputLengthRange.password.max) return await Lang.getFunctionPack("TOOLONG", [name, inputLengthRange.password.min, inputLengthRange.password.max], lang)
        return false
    }
    static async  confirmation(lang, name, content) {
        content = content.trim()
        if (content == "") return await Lang.getFunctionPack("NOTHING", [name], lang)
        return false
    }
}