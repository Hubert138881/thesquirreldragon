import React, {useState} from 'react'
import TypeCheckers from "../../server/collectors/TypeCheckers"
import Lang from "../collectors/LangCollector";
import Session from "../collectors/SessionCollector";
import languages from "../utilities/languages.json"

export default (props, Beginning, Main, Ending) => {
    const [showProfile, setShowProfile] = useState(false)
    if(!Beginning) Beginning = () => <></>
    if(!Ending) Ending = () => <></>
    if(!(Main().props.children[0] || Main().props.children.props)) return(<></>)
    else return (
        <>
            <ul>
                {props.userData.clientId && props.userData.termsAccepted ? <li><a href={`/${props.query.lang}`}>Main Room</a></li> : <li><a href={""}>Home</a></li>}
                <li><a href={""}>About Us</a></li>
                <li><a href={""}>Where to start?</a></li>
                <li><a href={""}>Contact</a></li>
            </ul>
            {!props.userData.termsAccepted ? <select name={"lang"}>
                {(() => {
                    let langOptions = [];
                    languages.map(lang => langOptions.push(<option value={lang.code} selected={props.query.lang == lang.code ? true : false} onClick = {(e) => e.target.value == props.query.lang ? null : Lang.setLang(lang.code, 30)}>{lang.name}</option>))
                    return langOptions
                })()}
            </select> : null}
            <br/>
            {
                !props.query["welcomeBar"] ?
                    props.userData.clientId ?
                        <i>{props.langPack["welcome-you"]}: {props.userData.email}</i> :
                        <>
                            <button onClick={() => location.href = `/${props.query.lang}/login`}>Login</button>
                            <button onClick={() => location.href = `/${props.query.lang}/registration`}>Register</button>
                        </> :
                 null
            }
            {
                !props.query["welcomeBar"] ?
                    props.userData.termsAccepted ? <>
                    <button onClick={() => setShowProfile(!showProfile)}>v</button>
                    <div id={"rightbar"} style={showProfile ? {} : {display: "none"}}>
                        <h3>O twoim profilu</h3>
                        <a href={`/${props.query.lang}/change-user-data`}><button>Settings</button></a>
                        <button onClick={() => Session.delete().then(
                            () => {
                                alert(props.langPack["logged-out"])
                                location.href = "/"
                            },
                            () => alert(props.langPack["E1005"])
                        )}>{props.langPack.logout}</button>
                        <br/>
                        <img src={props.userData.profilePictureExt ? `/files/users/${props.userData.clientId}/profile_picture.${props.userData.profilePictureExt}` : `/files/users/default_profile_picture.png`} width={"200px"} alt={""}/>
                        <p className={"my_profile_name"}>{props.userData.username} ({props.userData.email})</p>
                        <a href={``}><button>Sprawdź liste swoich odpowiedzianych pytań</button></a>
                    </div>
                </> : null
                    : null
            }
            <br/>
            {TypeCheckers(null, null, "moderator", props.userData.type) ? <>
                <a href={`/${props.query.lang}/moderation-panel`}>Moderate questions</a>
                <a href={`/${props.query.lang}/console`}>Console</a>
            </> : <></>}

            <Beginning />

            <main><Main/></main>

            <Ending/>
        </>
    )
}